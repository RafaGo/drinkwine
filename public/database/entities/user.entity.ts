import DefaultEntityType from "./default";

export default interface User extends DefaultEntityType {
    id?:string,
    nome: string,
    cpf: string,
    email: string,
    senha?: string ,    
    grupo: string
    ativo: boolean
    desabilitado?:false
}