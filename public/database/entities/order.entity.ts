import CartInterface from "@/interfaces/cart.interface";
import DefaultEntityType from "./default"

export default interface Order extends DefaultEntityType {
  address: any,
  clientId: string; 
  products: any;
  status: string,
  paid: boolean, 
  metodoPagamento: 'cartao' | 'boleto',
  valorTotal: number
  numeroPedido: string,
  valorFrete: number,
  valorCarrinho: number,
}