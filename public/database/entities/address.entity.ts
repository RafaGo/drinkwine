export default interface Address {
    ativo?:boolean;
    cidade: string;
    uf: string;
    bairro: string;
    cep: string;
    numero: string;
    complemento: string;
    logradouro: string;
    tipoEndereco?: string;
    padrao?: boolean
    faturamento?: boolean

}