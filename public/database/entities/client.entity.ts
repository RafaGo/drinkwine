import Address from "./address.entity";
import DefaultEntityType from "./default";

export default interface Client extends DefaultEntityType {
    nome: string;
    nascimento: string;
    sexo: string;
    cpf: string,
    email: string,
    senha?: string 
}
