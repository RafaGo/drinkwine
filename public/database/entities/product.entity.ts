import DefaultEntityType from "./default"

export default interface Product extends DefaultEntityType {
    nome: string,
    avaliacao: string,
    descricao: string,
    preco: string,
    qtd: string,
    ativo: boolean,
    image?: Blob[],
    image_url?: string[],
    image_ref?: string[], 
    image_refs ?: string[],

    imageMain ?: Blob,
    imageMain_url ?: string,
    imageMain_ref ?: string
}