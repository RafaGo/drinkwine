import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyCwMXpTBZ7ciaPmgoEOMmWb_9NtTgivc9s",
  authDomain: "drinkwine-2327f.firebaseapp.com",
  projectId: "drinkwine-2327f",
  storageBucket: "drinkwine-2327f.appspot.com",
  messagingSenderId: "631994483957",
  appId: "1:631994483957:web:6de7831411c900cbb331bc",
  measurementId: "G-DMSX2L9SXS"
}; 


const app = initializeApp(firebaseConfig);
const firestore = getFirestore(app); 
const auth = getAuth(app);



export { app,firestore,auth}; 