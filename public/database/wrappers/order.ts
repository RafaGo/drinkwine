import Order from "../entities/order.entity";
import { QueryConstraint, Unsubscribe } from "firebase/firestore";
import FirestorePipe from "../pipe";

class OrderDB extends FirestorePipe {
  public static readonly colName = 'Orders'

  
  constructor() {
    super(OrderDB.colName);
}
  public create(data: Order): Promise<any> {
    return this._create(data);
  }

  public update(id: string, data: Order): Promise<any> {
    return this._update(id, data);
  }

  public delete(id: string): Promise<any> {
    return this._delete(id);
  }

  public getAll(...params: QueryConstraint[]): Promise<Order[]> {
    return this._getAll<Order>(...params);
  }

  public get(id: string): Promise<Order> {
    return this._get(id);
  }

  public on(listener: (data: Order[]) => void, ...params: QueryConstraint[]): Unsubscribe {
    return this._on(listener, ...params);
  }

}

export default OrderDB;
