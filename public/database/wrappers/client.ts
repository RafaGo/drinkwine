import { QueryConstraint, Unsubscribe } from "firebase/firestore";
import FirestorePipe from "../pipe";
import { getDocs,query,where,limit } from "firebase/firestore";

import Client from "../entities/client.entity";

class ClientDB extends FirestorePipe {

    public static readonly colName = 'Clients'

    constructor() {
        super(ClientDB.colName);
    }

    async getByEmail(email: string): Promise<Client | null> {
        const snapshot = await getDocs(query(this.colRef, where('email', '==', email), limit(1)))
        if (snapshot.empty)
            return null;

        const doc = snapshot.docs[0];
        return {
            id: doc.id,
            ...doc.data(),
        } as any;
    }
    public createWithCustomId(id: string, data: Client): Promise<any> {
        return this._define(id, data);
    }
    public create(data: Client): Promise<any> {
        return this._create(data);
    }
    public update(id: string, data: Client): Promise<any> {
        return this._update(id, data);
    }
    public delete(id: string): Promise<any> {
        return this._delete(id);
    }
    public getAll(...params: QueryConstraint[]): Promise<Client[]> {
        return this._getAll<Client>(...params);
    }
    public get(id: string): Promise<Client> {
        return this._get(id);
    }
    public on(listener: (data: Client[]) => void, ...params: QueryConstraint[]): Unsubscribe {
        return this._on(listener, ...params);
    }

}

export default ClientDB;