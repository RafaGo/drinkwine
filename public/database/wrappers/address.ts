import { QueryConstraint, Unsubscribe } from "firebase/firestore";
import FirestorePipe from "../pipe";
import Address from "../entities/address.entity";
import ClientDB from "./client";

class AddressDB extends FirestorePipe {

    public static readonly colName = 'Addresses'

    constructor(idClient: string) {
        if (!idClient)
            throw new Error('Informe o id do cliente')
        super(`${ClientDB.colName}/${idClient}/${AddressDB.colName}`);
    }

    public create(data: Address): Promise<any> {
        return this._create(data);
    }

    public update(id: string, data: Address): Promise<any> {
        return this._update(id, data);
    }

    public delete(id: string): Promise<any> {
        return this._delete(id);
    }

    public getAll(...params: QueryConstraint[]): Promise<Address[]> {
        return this._getAll<Address>(...params);
    }

    public get(id: string): Promise<Address> {
        return this._get(id);
    }

    public on(listener: (data: Address[]) => void, ...params: QueryConstraint[]): Unsubscribe {
        return this._on(listener, ...params);
    }

}

export default AddressDB;
