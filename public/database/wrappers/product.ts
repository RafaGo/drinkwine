import { QueryConstraint, Unsubscribe } from "firebase/firestore";
import FirestorePipe from "../pipe"
import Product from "../entities/product.entity";

class ProductDB extends FirestorePipe {
    public static readonly colName = 'Products'

    constructor() {
        super(ProductDB.colName);
    }

    public async create(data: Product): Promise<any> {
        try {
            await this.saveFile(data);
            await this.saveFileMain(data, 'Main');
            return await this._create(data);
        } catch (error: any) {
            if (data.imageMain_ref) await this.deleteFile(data.imageMain_ref)
            if (data.image_refs) {
                for (const imageRef of data.image_refs) {
                    await this.deleteFile(imageRef);
                }
            }
            throw error.message;
        }
    }
    public async update(id: string, data: Product): Promise<any> {
        try {
            await this.saveFile(data);
            await this.saveFileMain(data, 'Main');
            return await this._update(id, data);
        } catch (error: any) {
            if (data.imageMain_ref) await this.deleteFile(data.imageMain_ref)
            if (data.image_refs) {
                for (const imageRef of data.image_refs) {
                    await this.deleteFile(imageRef);
                }
            }
            throw error.message;
        }
    }
    public delete(id: string): Promise<any> {
        return this._delete(id);
    }
    public getAll(...params: QueryConstraint[]): Promise<Product[]> {
        return this._getAll<Product>(...params);
    }
    public get(id: string): Promise<Product> {
        return this._get(id);
    }
    public on(listener: (data: Product[]) => void, ...params: QueryConstraint[]): Unsubscribe {
        return this._on(listener, ...params);
    }
    private async saveFile(data: Product) {
        if (data.image && data.image.length > 0) {
            const uploadPromises = data.image.map(async (img) => {
                const resultUpload = await this.uploadFile(new Date().getTime().toString(), img);
                return { ref: resultUpload.ref.fullPath, url: resultUpload.url };
            });
            const results = await Promise.all(uploadPromises);
            data.image_ref = results.map(result => result.ref);
            data.image_url = results.map(result => result.url);
        }
        delete data.image;
    }

    private async saveFileMain(data: any, label: string) {
        if (data['image' + label]) {
            const resultUpload = await this.uploadFile(new Date().getTime().toString(), data['image' + label]);
            data['image' + label + '_ref'] = resultUpload.ref.fullPath;
            data['image' + label + '_url'] = resultUpload.url;
        }
        delete data['image' + label]
    }

}

export default ProductDB;