import React, { createContext, useCallback, useMemo, useState, useEffect } from "react";


const CartContext = createContext({});


export const CartProvider = ({ children }) => {

  const [cart, setCart] = useState([]);
  const [freteValue, setFreteValue] = useState(0);
  useEffect(() => {
    const fetchProductsFromSessionStorage = async () => {
      const productsInCart = await sessionStorage.getItem('productsInCart');
      const products = productsInCart ? JSON.parse(productsInCart) : [];
      products.forEach(product => {
        setCart(state => ([...state, {
          id: product.id,
          nome: product.nome,
          qtd: product.qtd,
          preco: product.preco,
          imageMain_url: product.imageMain_url
        }]));
      });
    };

    fetchProductsFromSessionStorage();
  }, []);

  function stringToNumber(value) {
    const cleanedValue = value.replace(',', '.');
    return parseFloat(cleanedValue);
  }


  const valueInCart = useMemo(() => cart.reduce((prev, curr) => prev + (curr.qtd * (stringToNumber(curr.preco))), 0), [
    cart]);

  const totalComFrete = useMemo(() => valueInCart + freteValue, [valueInCart, freteValue]);
  const qtdInCart = useMemo(() => cart.reduce((prev, curr) => prev + curr.qtd, 0), [cart]);

  const addProductInCart = useCallback(async (product) => {
    const productObj = {
      ...product,
      qtd: 1,
    };
    addProductInStorage(productObj);
  }, []);


  const addProductInStorage = useCallback(async (product) => {
    const productsInCart = await sessionStorage.getItem('productsInCart');
    let products = productsInCart ? JSON.parse(productsInCart) : [];
    const productIndex = products.findIndex((p) => p.id === product.id);
    if (productIndex !== -1) {
      products[productIndex].qtd++;
    } else {
      products.push(product);
    }
    sessionStorage.setItem('productsInCart', JSON.stringify(products));
    setCart([...products]);
  }, []);



  const handleRemoveItem = useCallback(async (index) => {
    const productsInCart = await sessionStorage.getItem('productsInCart');
    const products = productsInCart ? JSON.parse(productsInCart) : [];
    if (!isNaN(index) && index >= 0 && index < products.length) {
      products.splice(index, 1);
      sessionStorage.setItem('productsInCart', JSON.stringify(products));
      setCart([...products]);
    }

  }, []);



  const decrementProduct = useCallback(async (product) => {
    const productsInCart = await sessionStorage.getItem('productsInCart');
    const products = productsInCart ? JSON.parse(productsInCart) : [];
    const pIndex = products.findIndex((p) => p.id === product.id);
    console.log(pIndex);
    if (pIndex !== -1) {
      products[pIndex].qtd = products[pIndex].qtd - 1;
      sessionStorage.setItem('productsInCart', JSON.stringify(products));
      setCart([...products]);
    }
  }, [])


  const clearCart = useCallback(() => {
    sessionStorage.clear();
    setCart([]);
  }, []);

  return (
    <CartContext.Provider value={{
      cart,
      addProductInCart,
      decrementProduct,
      valueInCart,
      qtdInCart,
      setCart,
      handleRemoveItem,
      freteValue,
      setFreteValue,
      totalComFrete,
      clearCart

    }}>
      {children}
    </CartContext.Provider>

  )
}

export default CartContext;