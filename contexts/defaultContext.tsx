import DefaultContextInterface from '@/interfaces/default.interface';
import Client from '@/public/database/entities/client.entity';
import Product from '@/public/database/entities/product.entity';
import User from '@/public/database/entities/user.entity';
import ClientDB from '@/public/database/wrappers/client';
import ProductDB from '@/public/database/wrappers/product';
import { where } from 'firebase/firestore';
import { useSession } from 'next-auth/react';
import { createContext, useEffect, useState } from 'react';

export const DefaultContext = createContext<DefaultContextInterface | null>(null);

export default function DefaultProvider({ children }: { children: React.ReactNode }) {
    const { data: session } = useSession();
    const userSession: any = session;
    
    const [user, setUser] = useState<User | null>(null);
    const [products, setproducts] = useState<Product[]>([]);
    const [clients, setclients] = useState<Client | null>(null);




    useEffect(() => {
        const fetchData = async () => {
            if (userSession && userSession.token ) {
                setUser({
                    nome: userSession.token.nome,
                    grupo: userSession.token.grupo,
                    cpf: userSession.token.cpf,
                    email: userSession.token.email,
                    id: userSession.token.id,
                    senha: userSession.token.senha,
                    ativo: userSession.token.ativo
                }); 
            }
        };
        fetchData();
    }, [userSession]);

    useEffect(() => {
        const fetchData = async () => {
            if (userSession && userSession.token ) {
                setclients({
                    nome: userSession.token.nome,
                    nascimento: userSession.token.nascimento,
                    cpf: userSession.token.cpf,
                    email: userSession.token.email,
                    id: userSession.token.id,
                    senha: userSession.token.senha,
                    sexo: userSession.token.sexo
                }); 
            }
        };
        fetchData();
    }, [userSession]);

    useEffect(() => {
        const onSubscribe = new ProductDB().on(products => {
            setproducts(products)
        })
        return onSubscribe;
    }, []) 





    return (
        <DefaultContext.Provider value={{
            user,
            products,
            clients,
        }}>
            {children}
        </DefaultContext.Provider>
    );
}
