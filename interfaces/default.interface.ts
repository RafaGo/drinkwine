
import Client from "@/public/database/entities/client.entity";
import Product from "@/public/database/entities/product.entity";
import User from "@/public/database/entities/user.entity";


export default interface DefaultContextInterface {
    user: User | null,
    products:Product[],
    clients: Client | null,

}