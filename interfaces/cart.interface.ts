import Product from "@/public/database/entities/product.entity";

export default interface CartInterface {
  cart: any,
  addProductInCart: (product: Product) => void,
  decrementProduct: (product: Product) => void,
  incrementProduct: (product: Product) => void,
  totalProductInCart: number,
  valueInCart: any
  qtdInCart: any
  setCart: any,
  handleRemoveItem: (index: number) => void,
  setFreteValue: any
  freteValue: any,
  totalComFrete: number,
  clearCart: () => void
}