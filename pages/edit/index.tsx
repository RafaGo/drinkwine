import React, { useState } from 'react';
import { Card } from 'primereact/card';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';  
import { Password } from 'primereact/password';
import { InputMask } from 'primereact/inputmask';
import router, { useRouter } from "next/router";
import '../app.css';       
import './index.css';

const EditPage = () => {
    const [isNomeEditable, setIsNomeEditable] = useState(false);
    const [isCpfEditable, setIsCpfEditable] = useState(false);
    const [isSenhaEditable, setIsSenhaEditable] = useState(false);
    const [isConfirmarSenhaEditable, setIsConfirmarSenhaEditable] = useState(false);

    const handleEditClick = (fieldName: string) => {
        switch (fieldName) {
            case 'nome':
                setIsNomeEditable(!isNomeEditable);
                break;
            case 'cpf':
                setIsCpfEditable(!isCpfEditable);
                break;
            case 'senha':
                setIsSenhaEditable(!isSenhaEditable);
                break;
            case 'confirmar-senha':
                setIsConfirmarSenhaEditable(!isConfirmarSenhaEditable);
                break;
            default:
                break;
        }
    };

    const handleCadastro = () => {
        router.push('/');
    };

    return (
        <div className='cont'>
            <Card className='card-edit'>
                <h1>Alterar Usuário</h1>
                <div className="flex gap-4 flex-wrap justify-content-center mt-8">
                    <div className="flex flex-column gap-2">
                        <label htmlFor="nome">Nome</label>
                        <div className='flex'>
                            <InputText id="nome" disabled={!isNomeEditable} />
                            <Button icon="pi pi-pencil" className='ml-2' onClick={() => handleEditClick('nome')} />
                        </div>
                    </div>
                    <div className="flex flex-column gap-2">
                        <label htmlFor="cpf">CPF</label>
                        <div className='flex align-items-center'>
                            <InputMask  mask="999.999.999-99" id="cpf" disabled={!isCpfEditable} />
                            <Button icon="pi pi-pencil" className='ml-2' onClick={() => handleEditClick('cpf')} />
                        </div>
                    </div>
                    <div className="flex flex-column gap-2">
                        <label htmlFor="senha">Senha</label>
                        <div className='flex align-items-center'>
                            <Password id="senha" type="password" toggleMask feedback={false} disabled={!isSenhaEditable} />
                            <Button icon="pi pi-pencil" className='ml-2' onClick={() => handleEditClick('senha')} />
                        </div>
                    </div>
                    <div className="flex flex-column gap-2">
                        <label htmlFor="confirmar-senha">Confirmar senha</label>
                        <div className='flex align-items-center'>
                            <Password id="confirmar-senha" type="password" toggleMask feedback={false} disabled={!isConfirmarSenhaEditable} />
                            <Button icon="pi pi-pencil" className='ml-2' onClick={() => handleEditClick('confirmar-senha')} />
                        </div>
                    </div>
                </div>
                <div className='cont-btn mt-8'>
                    <Button label='Cancelar' className='btn-cancelar' />
                    <Button label='Salvar' className='btn-salvar' />
                </div>
            </Card>
        </div>
    );
};

export default EditPage;
