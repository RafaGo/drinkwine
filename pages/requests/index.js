import { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { Button } from "primereact/button";
import { Tag } from "primereact/tag";
import "./style.css";
import "../app.css";
import { useSession } from "next-auth/react";
import OrderDB from "/public/database/wrappers/order";
import dateFormat from "dateformat";
import { orderBy, where } from "firebase/firestore";
import { useRouter } from "next/router";
import HeaderCliente from "@/components/HeaderCliente";
import { ProgressSpinner } from "primereact/progressspinner";

const statusColors = {
  "Aguardando pagamento": "yellow",
  "Pagamento rejeitado": "red",
  "Pagamento com sucesso": "green",
  "Aguardando retirada": "blue",
  "Em trânsito": "orange",
  "Entregue": "purple",
};

const Requests = () => {
  const router = useRouter()
  const [orders, setOrders] = useState([]);
  const { data: session } = useSession();
  const formattedNome = session?.token?.nome
    ? session.token.nome.split(" ")[0].charAt(0).toUpperCase() + session.token.nome.split(" ")[0].slice(1).toLowerCase()
    : "Usuário";
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchOrders = async () => {
      try {
        setLoading(true);
        const clientId = session?.token?.id;
        if (clientId) {
          const orderDB = new OrderDB();
          const allOrders = await orderDB.getAll(where("clientId", "==", clientId), orderBy("created_at", "desc"));
          setOrders(allOrders);
        }
      } catch (error) {
        console.error("Erro ao buscar pedidos:", error);
      } finally {
        setLoading(false);
      }
    };
    fetchOrders();
  }, [session]);

  const handleDetailsClick = (orderId) => {
    router.push(`/detailsOrder/${orderId}`);
  };

  const handleVoltar = () => {
    router.push("/");
  };

  return (
    <div>
      <HeaderCliente userName={formattedNome} />
      {loading ? (
          <div className="card flex justify-content-center align-items-center" style={{ height: "100vh", background: "#1E1E1E" }}>
            <ProgressSpinner />
          </div>
        ) : (
      <Card className="cont-pedidos">
        <div className="flex flex-column gap-4 mt-5">
          <h1 className="mb-5 title">Meus pedidos</h1>
          {orders.length > 0 ? (
            orders.map((order) => (
              <div key={order.id} className="card-pedidos">
                <div className="flex w-full h-full justify-content-between">
                  <div className="flex flex-column justify-content-between">
                    <div className="flex align-items-center gap-2">
                      <h2 className="m-0">Número do pedido:</h2>
                      <p className="m-0">{order.numeroPedido}</p>
                    </div>
                    <div className="flex align-items-center gap-2">
                      <h2 className="m-0">Data da compra:</h2>
                      {order.created_at ? (
                        <p className="m-0 ">{dateFormat(order.created_at.toDate(), "dd/mm/yyyy")}</p>
                      ) : (
                        <p className="m-0">Data desconhecida</p>
                      )}
                    </div>
                    <div className="flex align-items-center gap-2">
                      <h2 className="m-0">Total da compra:</h2>
                      <p className="m-0">
                        {order.valorTotal.toLocaleString("pt-BR", { style: "currency", currency: "BRL" })}
                      </p>
                    </div>
                  </div>
                  <div className="flex flex-column justify-content-between align-items-end">
                    <div className="flex align-items-center gap-2">
                      <h2 className="m-0">Status</h2>
                      <Tag style={{ backgroundColor: statusColors[order.status] }}>
                        {order.status}
                      </Tag>
                    </div>
                    <Button label="Detalhes" className="btn-detalhes" onClick={() => handleDetailsClick(order.id)} />
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="cont-mensagem">
              <i className="pi pi-box" style={{ fontSize: "2rem" }}></i>
              <p>Você não tem nenhum pedido</p>
              <Button label="Voltar" onClick={handleVoltar} />
            </div>
          )}
        </div>
      </Card>
      )}
    </div>
  );
};

export default Requests;
