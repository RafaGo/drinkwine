import { NextPage } from "next";
import { useRouter } from "next/router";
import { signOut } from "next-auth/react";
import { useSession } from "next-auth/react";
import { Card } from 'primereact/card';
import '../app.css';
import './index.css';
import { auth } from "@/public/database/firebase";
import { useContext, useEffect } from "react";
import Header from "@/components/Header";
import { DefaultContext } from "@/contexts/defaultContext";

const Backoffice = () => {
    const { user } = useContext(DefaultContext);

    const router = useRouter();

    const { data: session } = useSession();


    const BuscaUsuario = () => {
        router.push('/buscarUsuario');
    }

    const sair = () => {
        signOut();
        router.push('/login');
    }

    const cadastroProduto = () => {
        router.push('/cadastroProduto');
    }

    console.log("Conteúdo da sessão:", session);

    const formattedNome = session?.token?.nome ? session.token.nome.split(' ')[0].charAt(0).toUpperCase() + session.token.nome.split(' ')[0].slice(1).toLowerCase() : 'Usuário';

    return (
        <div>
            <Header userName={formattedNome} />
            <div className="cont-backoffice">
                <Card className="card-backoffice">
                    <h1 className="mt-7">Bem-vindo ao Backoffice, {formattedNome}!</h1>
                </Card>
            </div>
        </div>
    );
};

export default Backoffice;
