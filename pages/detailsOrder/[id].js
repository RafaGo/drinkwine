import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { useSession } from "next-auth/react";
import OrderDB from "@/public/database/wrappers/order";
import dateFormat from "dateformat";
import { Tag } from "primereact/tag";
import Order from "@/public/database/entities/order.entity";
import "../app.css";
import "./index.css";
import HeaderCliente from "@/components/HeaderCliente";
import { ProgressSpinner } from "primereact/progressspinner";

const statusColors = {
  "Aguardando pagamento": "yellow",
  "Pagamento rejeitado": "red",
  "Pagamento com sucesso": "green",
  "Aguardando retirada": "blue",
  "Em trânsito": "orange",
  "Entregue": "purple",
};

const OrderDetails = () => {
  const router = useRouter();
  const [order, setOrder] = useState  ();
  const { data: session } = useSession();
  const formattedNome = session?.token?.nome ? session.token.nome.split(" ")[0].charAt(0).toUpperCase() + session.token.nome.split(" ")[0].slice(1).toLowerCase() : "Usuário";

  useEffect(() => {
    const { id: orderId } = router.query;
    const fetchOrder = async () => {
      try {
        if (orderId && session?.token?.id) {
          const orderDB = new OrderDB();
          const fetchedOrder = await orderDB.get(orderId);
          setOrder(fetchedOrder);
        }
      } catch (error) {
        console.error("Erro ao buscar detalhes do pedido:", error);
      }
    };
    fetchOrder();
  }, [session]);



  if (!order) return <div className="card flex justify-content-center align-items-center" style={{ height: "100vh", background: "#1E1E1E" }}>
    <ProgressSpinner />
  </div>;

  return (
    <div>
      <HeaderCliente userName={formattedNome} />
      <Card className="cont-resume">
        <h1>Detalhes do Pedido</h1>
        <div className="flex gap-6 cont-data border-300 border-bottom-1 px-4">
          <div className="flex align-items-center gap-2">
            <h2>Número do pedido:</h2>
            <p className="m-0">{order.numeroPedido}</p>
          </div>
          <div className="flex align-items-center gap-2">
            <h2>Data da compra:</h2>
            {order.created_at ? (
              <p className="m-0">{dateFormat(order.created_at.toDate(), "dd/mm/yyyy")}</p>
            ) : (
              <p className="m-0">Data desconhecida</p>
            )}
          </div>
        </div>
        <div className="flex gap-5 align-items-center cont-status">
          <h2 className="m-0">Status:</h2>
          <Tag style={{ backgroundColor: statusColors[order.status] }}>
            {order.status}
          </Tag>
        </div>
        <div className="flex gap-3 mt-5">
          <div className="flex flex-column gap-3">
            <h2 className="mb-0" style={{ marginTop: "34px" }}>Produtos:</h2>
            {order.products.map(product => (
              <Card key={product.id} className="products">
                <div className="flex gap-2 align-items-center">
                  <div className="cont-img">
                    <img src={product.imageMain_url} alt={product.nome} />
                  </div>
                  <div>
                    <h2 className="m-0">{product.nome}</h2>
                    <p className="m-0 mt-2 flex align-items-center gap-1">Quantidade: <p className="quant">{product.qtd}</p></p>
                    <p className="m-0 mt-2 flex align-items-center gap-1">Valor unitário: <p className="quant">{product.preco}</p></p>

                  </div>
                </div>

                <div>
                  <p className="m-0 mt-2">R$ {(parseFloat(product.preco) * product.qtd).toFixed(2)}</p>
                </div>
              </Card>
            ))}
          </div>
          <div className="flex flex-column gap-3">
            <div className="value mt-8 flex flex-column justify-content-between">
              <div>
                <h1>Valor da compra</h1>
                <div className="flex justify-content-between align-items-center mt-6">
                  <h2>Valor dos Produtos:</h2>
                  <p>R$ {order.valorCarrinho.toFixed(2)}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                  <h2>Valor do Frete:</h2>
                  <p>{Number.isInteger(order.valorFrete) ? `${order.valorFrete}.00` : order.valorFrete}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                  <h2>Total da compra:</h2>
                  <p>R$ {order.valorTotal.toFixed(2)}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                  <h2>Método de pagamento:</h2>
                  <p className="metodo-pagamento">{order.metodoPagamento}</p>
                </div>
              </div>
            </div>
            <div className="delivery">
              <h1>Endereço de entrega</h1>
              <div className="flex justify-content-between align-items-center mt-6">
                <h2>Rua:</h2>
                <p>{order.address.logradouro}</p>
              </div>
              <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                <h2>Número:</h2>
                <p>{order.address.numero}</p>
              </div>
              <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                <h2>Complemento:</h2>
                <p>{order.address.complemento}</p>
              </div>
              <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                <h2>Bairro:</h2>
                <p>{order.address.bairro}</p>
              </div>
              <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                <h2>CEP:</h2>
                <p>{order.address.cep}</p>
              </div>
              <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                <h2>Cidade:</h2>
                <p>{order.address.cidade}</p>
              </div>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default OrderDetails;
