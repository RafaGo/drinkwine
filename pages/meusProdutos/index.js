import { useContext, useEffect, useState } from 'react';
import { DefaultContext } from '@/contexts/defaultContext';
import { Button } from 'primereact/button';
import { Carousel } from 'primereact/carousel';
import ModalDetailsProduct from "@/components/modals/ModalDetailsProduct";
import './style.css'

const MeusProdutos = () => {
    const { products } = useContext(DefaultContext);
    const [productsFilter, setProductsFilter] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(null);

    useEffect(() => {
        if (products && products.length > 0) {
            setProductsFilter(products);
        }
    }, [products]);

    const showProductDetails = (product) => {
        setSelectedProduct(product);
        setShowModal(true);
    };

    const responsiveOptions = [
        {
            breakpoint: '1400px',
            numVisible: 4,
            numScroll: 4
        },
        {
            breakpoint: '1199px',
            numVisible: 4,
            numScroll: 4
        },
        {
            breakpoint: '767px',
            numVisible: 2,
            numScroll: 2
        },
        {
            breakpoint: '575px',
            numVisible: 1,
            numScroll: 1
        }
    ];

    const productTemplate = (product) => {
        return (
            <div className="m-2 text-center p-3 divCard flex flex-column justify-content-between">
                <div className="imgArea">
                    <img src={product.imageMain_url} alt={product.nome} className="w-full productImage" loading='lazy'/>
                </div>
                <div className='mb-2'>
                    <h4 className="nameProduct">{product.nome}</h4>
                    <h6 className="mt-2 priceProduct">${product.preco}</h6>
                </div>
                <div className="flex justify-content-center btnDetalhes">
                    <Button icon="pi pi-arrow-up-right" iconPos="right" className="p-button p-button-rounded" label='Detalhes' onClick={() => showProductDetails(product)}/>
                </div>
            </div>
        );
    };

    return (
        <div className="card mt-5">
            <Carousel value={products} numScroll={4} numVisible={5} indicatorsContentClassName='disable' responsiveOptions={responsiveOptions} itemTemplate={productTemplate} circular autoplayInterval={4000} />
            {selectedProduct && (
                <ModalDetailsProduct
                    visible={showModal}
                    setIsClose={() => setShowModal(false)}
                    productDataView={selectedProduct}
                />
            )}
        </div>
                    /* {productsFilter.map((product) => (
                        <div key={product.id} className='shadow rounded-t-md h-full'>
                            
                            
                            <div className='p-2'>
                                <p className='text-center uppercase font-bold text-xl overflow-hidden whitespace-nowrap'>
                                    {product.nome.length > 10 ? `${product.nome.substring(0, 10)}...` : product.nome}
                                </p>
                            </div>
                            <div className='p-2'>
                                <p className='text-center uppercase font-bold text-xl overflow-hidden whitespace-nowrap'>
                                    {product.preco}
                                </p>
                            </div>
                        </div>
                    ))} */
    )
}

export default MeusProdutos;
