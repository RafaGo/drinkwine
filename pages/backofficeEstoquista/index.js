import { signOut, useSession } from "next-auth/react";
import { NextPage } from "next";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import '../app.css';
import './index.css';
import { useRouter } from "next/router";
import { useEffect } from "react";
import HeaderEstoquista from "@/components/HeaderEstoquista";

const backofficeEstoquista = () => {

    const router = useRouter();

    const { data: session } = useSession();

 
    const sair = () => {
        signOut();
        router.push('/login');
    }

    const formattedNome = session?.token?.nome ? session.token.nome.split(' ')[0].charAt(0).toUpperCase() + session.token.nome.split(' ')[0].slice(1).toLowerCase() : 'Usuário';

    return (
        <div>
            <HeaderEstoquista userName={formattedNome} />
            <div className="cont-backoffice">
                <Card className="card-backoffice">
                    <h1 className="mt-7">Bem-vindo ao Backoffice, {formattedNome}!</h1>
                </Card>
            </div>
        </div>
    );
};


export default backofficeEstoquista;
