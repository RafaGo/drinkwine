import { useState, useEffect, useCallback } from 'react';
import { Formik, Form, Field, FieldProps, ErrorMessage, useFormik } from 'formik';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';

import ProductDB from '@/public/database/wrappers/product';
import { useRouter } from 'next/router';

const CadastroProduto = () => {
  const [loading, setLoading] = useState(false);
  const [editData, setEditData] = useState  (null);
  const router = useRouter()

  const formik = useFormik({
    initialValues: {
      nome: '',
      avaliacao: '',
      descricao: '',
      ativo: true,
      preco: '',
      qtd: '',
      image: [],
      image_url: [],
      imageMain: [],
      imageMain_url: []
    },
    onSubmit: async (values) => {
      setLoading(true);
      const { nome, avaliacao, descricao, ativo, preco, qtd, image_url, image, imageMain, imageMain_url } = values;
      const data = {
        nome,
        avaliacao,
        descricao,
        ativo,
        preco,
        qtd,
        image_url,
        image,
        imageMain,
        imageMain_url,
      };

      console.log(data);
      if (editData) {
        await new ProductDB().update(editData.id, data);
        setLoading(false);
        console.log('Atualizado');
      } else {
        await new ProductDB().create(data).then((e) => console.log(e)).catch((e) => console.log(e));
        setLoading(false);
        console.log('Criado');
        router.push('/buscaProduto')
        console.log('aqui', data.image_url)
      }
    },
  });

  useEffect(() => {
    setLoading(false);
    formik.resetForm();
    if (editData) {
      const { nome, avaliacao, descricao, ativo, preco, qtd, image_url, imageMain_url } = editData;
      formik.setValues({
        nome,
        avaliacao,
        descricao,
        ativo,
        preco,
        qtd,
        image: [],
        image_url,
        imageMain: [],
        imageMain_url,
      });
    }
  }, [editData]);

  const handleMainImage = useCallback((e) => {
    const [file] = Array.from(e.target.files);
    formik.setValues({
      ...formik.values,
      imageMain: file,
      imageMain_url: URL.createObjectURL(file)

    });

  }, [formik.values]);
  const handleOtherImages = useCallback((e) => {
    if (e.target && e.target.files) {
      const files = Array.from(e.target.files);
      const newImageUrls = files.map((file) => URL.createObjectURL(file));

      formik.setValues({
        ...formik.values,
        image: [...formik.values.image, ...files],
        image_url: [...formik.values.image_url, ...newImageUrls],
      });
    }
  }, [formik.values]);

  const handleCancel = () => {
    router.push('/buscaProduto');
  };



  return (
    <div className="p-fluid p-grid p-formgrid">
      <div className="p-field p-col-12">
        <label htmlFor="nome">Nome</label>
        <InputText id="nome" {...formik.getFieldProps('nome')} />
      </div>
      <div className="p-field p-col-12">
        <label htmlFor="avaliacao">Avaliação</label>
        <InputText id="avaliacao" {...formik.getFieldProps('avaliacao')} />
      </div>
      <div className="p-field p-col-12">
        <label htmlFor="preco">Preço</label>
        <InputText id="preco" {...formik.getFieldProps('preco')} />
      </div>
      <div className="p-field p-col-12">
        <label htmlFor="qtd">Quantidade</label>
        <InputText id="qtd" {...formik.getFieldProps('qtd')} />
      </div>
      <div className="p-field p-col-12">
        <label htmlFor="descricao">Descrição</label>
        <InputText id="descricao" {...formik.getFieldProps('descricao')} />
      </div>
      <div className="p-field p-col-12">
        <label htmlFor="imageMain_url">Imagem Principal</label>
        <input id="imageMain_url" type="file" accept="image/*" onChange={handleMainImage} />
        {formik.values.imageMain_url && (
          <img src={formik.values.imageMain_url} alt="Imagem principal" />
        )}
      </div>
      <div className="p-field p-col-12">
        <label htmlFor="outras_imagens">Outras Imagens</label>
        <input id="outras_imagens" type="file" accept="image/*" multiple onChange={handleOtherImages} />
        {formik.values.image_url.map((imageUrl, index) => ( // Renderiza a partir do segundo elemento
          <img key={index} src={imageUrl} alt={`Imagem ${index}`} />
        ))}
      </div>
      <div className="p-field p-col-12">
        <Button label="Salvar" onClick={() => formik.handleSubmit()} />
        <Button label="Cancelar" onClick={handleCancel} />

      </div>
    </div>
  );
};

export default CadastroProduto;
