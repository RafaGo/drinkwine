import { NextPage } from "next";
import { Formik, Form, Field, ErrorMessage, FieldProps } from "formik";
import UserDB from "@/public/database/wrappers/user";
import User from "@/public/database/entities/user.entity";
import { hashSync } from "bcryptjs";
import router from "next/router";
import { createUserWithEmailAndPassword, fetchSignInMethodsForEmail } from "firebase/auth";
import { useState } from "react";
import { Card } from "primereact/card";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { Password } from "primereact/password";
import { Button } from "primereact/button";
import { InputMask } from "primereact/inputmask";
import "./index.css";
import { auth } from "@/public/database/firebase";



const CadastroUsuario = () => {
    const [errorMessage, setErrorMessage] = useState("");
    const [loading, setLoading] = useState(false);


    function TestaCPF(strCPF ) {
        var Soma;
        var Resto;
        Soma = 0;
        strCPF = strCPF.replace(/[^0-9]/g, "");
        if (strCPF == "00000000000") return false;

        for (var i = 1; i <= 9; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10))) return false;

        Soma = 0;
        for (var i = 1; i <= 10; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11))) return false;
        return true;
    }
    //validando campos
    const validate = async (values) => {
        values.cpf = values.cpf.replace(/[^0-9]/g, "");
        let cpfMask;
        cpfMask = values.cpf.match(/(\d{3})(\d{3})(\d{3})(\d{2})/);
        if (cpfMask !== null) values.cpf = `${cpfMask[1]}.${cpfMask[2]}.${cpfMask[3]}-${cpfMask[4]}`;
        const errors = {};

        if (!values.nome) {
            errors.nome = "Este campo é necessário";
        } else if (values.nome.length < 3) {
            errors.nome = "O nome precisa ter 3 caracteres ou mais";
        }
        if (values.senha !== values.confirmarSenha) {
            errors.confirmarSenha = "As senhas não coincidem";
        }
        if (!values.cpf) {
            errors.cpf = "Este campo é necessário";
        } else if (cpfMask == null) {
            errors.cpf = "Informe o CPF corretamente";
        } else if (TestaCPF(values.cpf) == false) errors.cpf = "Informe um CPF válido";
        if (!values.ativo) {
            errors.ativo = "Você precisa escolher o status do usuário";
        }
        if (!values.senha) {
            errors.senha = "Este campo é necessário";
        } else if (values.senha.length < 6) {
            errors.senha = "A senha precisa ter 6 caracteres";
        }
        if (!values.grupo) {
            errors.grupo = "Este campo é necessário";
        }
        if (!values.email) {
            errors.email = "Este campo é necessário.";
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = "Email inválido.";
        } else {
            try {
                const signInMethods = await fetchSignInMethodsForEmail(auth, values.email);
                if (signInMethods.length > 0) {
                    setErrorMessage("Este e-mail já está sendo usado.");
                    errors.email = "Este e-mail já está sendo usado.";
                } else {
                    setErrorMessage("");
                }
            } catch (error) {
                setErrorMessage("Este e-mail já está sendo usado.");
            }
        }

        return errors;
    };

    return (
        <div className="cont">
            <Card className="card-cadastro">
                <h1>Cadastro de Cliente</h1>
                <Formik
                    initialValues={{
                        nome: "",
                        cpf: "",
                        ativo: true,
                        grupo: "",
                        email: "",
                        senha: "",
                        confirmarSenha: ""
                    }}
                    validate={validate}
                    onSubmit={async (values, { setSubmitting, setErrors, setFieldValue }) => {
                        setLoading(true);
                        const unmaskCpf = values.cpf?.replace(/\D/g, "");

                        try {
                            const { nome, ativo, grupo, email, senha } = values;
                            const password = senha;
                            const senhaHash = hashSync(senha, 10);
                            const data = {
                                nome: nome,
                                cpf: unmaskCpf,
                                email: email,
                                senha: senhaHash,
                                grupo: grupo,
                                ativo: ativo
                            };
                            const userCredential = await createUserWithEmailAndPassword(
                                auth,
                                email,
                                password,
                            );
                            const user = userCredential.user;
                            if (user) {
                                new UserDB().createWithCustomId(user.uid, data).then(() => console.log("CADASTRADO"));
                                if (grupo === "admin") {
                                    router.push("/backoffice");
                                } else {
                                    router.push("/backofficeEstoquista");
                                }
                            }
                        } catch (error) {
                            console.error("Erro ao cadastrar usuário:", error);
                            if (error.code === "auth/email-already-in-use") {
                                setErrorMessage("Este e-mail já está sendo usado.");
                                setErrors({ email: "Este e-mail já está sendo usado." });
                            } else {
                                setErrorMessage("Ocorreu um erro ao cadastrar o usuário.");
                                setErrors({});
                            }
                        }
                        setSubmitting(false);
                        setLoading(false);
                    }}
                >
                    {({ values, isSubmitting, setFieldValue }) => (
                        <Form>
                            <div className="flex flex-column justify-content-center align-items-center gap-4 mt-7">
                                <div className="flex gap-4 flex-wrap justify-content-center">
                                    <div className="flex flex-column gap-2">
                                        <label htmlFor="Nome">Nome</label>
                                        <Field type="text" name="nome" render={({ field }) => <InputText {...field} id="Nome" />} />
                                        <ErrorMessage name="nome" component="div" className="text-danger" />
                                    </div>

                                    <div className="flex flex-column gap-2">
                                        <label htmlFor="cpf" className="form-label">CPF</label>
                                        <Field type="cpf" name="cpf" render={({ field }) => <InputMask mask="999.999.999-99" {...field} id="cpf" />} />
                                        <ErrorMessage name="cpf" component="div" className="text-danger" />
                                    </div>

                                    <div className="flex flex-column gap-2">
                                        <label htmlFor="email" className="form-label">Email</label>
                                        <Field type="email" name="email" render={({ field }) => <InputText {...field} id="email" />} />
                                        <ErrorMessage name="email" component="div" className="text-danger" />
                                    </div>

                                    <div className="flex flex-column gap-2">
                                        <label htmlFor="senha" className="form-label">Senha</label>
                                        <Field type="password" name="senha" render={({ field }) => <Password {...field} id="senha" toggleMask feedback={false} />} />
                                        <ErrorMessage name="senha" component="div" className="text-danger" />
                                    </div>

                                    <div className="flex flex-column gap-2">
                                        <label htmlFor="confirmarSenha" className="form-label">Confirmar Senha</label>
                                        <Field type="password" name="confirmarSenha" render={({ field }) => <Password {...field} id="confirmarSenha" toggleMask feedback={false} />} />
                                        <ErrorMessage name="confirmarSenha" component="div" className="text-danger" />
                                    </div>

                                    <div className="flex flex-column gap-2">
                                        <label>Grupo</label>
                                        <Dropdown
                                            name="grupo"
                                            id="grupo"
                                            className="select-group"
                                            value={values.grupo}
                                            onChange={(e) => setFieldValue("grupo", e.value)}
                                            options={[
                                                { label: "Estoquista", value: "estoquista" },
                                                { label: "Administrador", value: "admin" }
                                            ]}
                                        />
                                        <ErrorMessage name="grupo" component="div" className="text-danger" />
                                    </div>
                                </div>
                            </div>
                            <div className="mt-7 cont-btn">
                                <Button
                                    label="Cadastrar"
                                    disabled={isSubmitting || loading}
                                    className="btn-cadastrar"
                                    loading={loading}
                                    iconPos={"right"}
                                />
                            </div>
                        </Form>

                    )}
                </Formik>
            </Card>
        </div >
    );
};

export default CadastroUsuario;
