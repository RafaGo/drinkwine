import { useState, useEffect } from "react";
import UserDB from "/public/database/wrappers/user";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { InputText } from "primereact/inputtext";
import { Card } from "primereact/card";
import { Paginator } from "primereact/paginator";
import { Button } from "primereact/button";
import { InputSwitch } from "primereact/inputswitch";
import { Skeleton } from "primereact/skeleton";
import "../app.css";
import "./index.css";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import ModalEditUser from "/components/modals/ModalEditUser";
import Header from "/components/Header";

const BuscarUsuario = () => {
	const router = useRouter();
	const [first, setFirst] = useState(0);
	const [rows, setRows] = useState(10);
	const [searchTerm, setSearchTerm] = useState("");
	const [searchResults, setSearchResults] = useState();
	const [totalRecords, setTotalRecords] = useState(0);
	const [currentPage, setCurrentPage] = useState(0);
	const [loading, setLoading] = useState(true);

	const [open, setOpen] = useState(false);
	const [userData, setUserData] = useState();
	const { data: session } = useSession();

	useEffect(() => {
		async function fetchUsers() {
			try {
				const users = await new UserDB().getAll();

				const filteredUsers = users.filter(user =>
					removeAccents(user.nome.toLowerCase()).includes(removeAccents(searchTerm.toLowerCase()))
				);

				setTotalRecords(filteredUsers.length);

				const startIndex = currentPage * rows;
				const endIndex = startIndex + rows;
				const usersForPage = filteredUsers.slice(startIndex, endIndex);

				setSearchResults(usersForPage);

				setLoading(false);
			} catch (error) {
				console.error("Erro ao buscar usuários:", error);
			}
		}

		fetchUsers();
	}, [searchTerm, currentPage, rows]);

	const onPageChange = (event) => {
		setFirst(event.first);
		setCurrentPage(event.first / rows);
	};

	const removeAccents = (str) => {
		return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
	};

	const handleCloseEditUser = () => {
		setOpen(false);
	};

	const handleEditUser = (user) => {
		setUserData(user);
		setOpen(true);
	};

	const handleCadastro = () => {
		router.push("/cadastroUsuario");
	};

	const items = Array.from({ length: 10 }, (v, i) => i);

	const handleDesativar = async () => {
		if ( user) {
			const confirmation = window.confirm("Tem certeza de que deseja desativar este usuário?");
			if (confirmation) {
				try {
					await new UserDB().update(user.id, { ...user, ativo: false });

					setSearchResults(prevResults =>
						prevResults.map(prevUser =>
							prevUser.id === user.id ? { ...prevUser, ativo: false } : prevUser
						)
					);
				} catch (error) {
					console.error("Erro ao desativar usuário:", error);
				}
			}
		}
	};

	const handleAtivar = async () => {
		if (user.id) {
			const confirmation = window.confirm("Tem certeza de que deseja ativar este usuário?");
			if (confirmation) {
				try {
					await new UserDB().update(user.id, { ...user, ativo: true });

					setSearchResults(prevResults =>
						prevResults.map(prevUser =>
							prevUser.id === user.id ? { ...prevUser, ativo: true } : prevUser
						)
					);
				} catch (error) {
					console.error("Erro ao ativar usuário:", error);
				}
			}
		}
	};

	const formattedNome = session?.token?.nome ? session.token.nome.split(" ")[0].charAt(0).toUpperCase() + session.token.nome.split(" ")[0].slice(1).toLowerCase() : "Usuário";


	return (
		<div>
			<Header userName={formattedNome} />
			<Card className="card-list-members h-full">
				<div className="cont-list-users">
					<h1 className="mt-7">Lista de Usuários</h1>

					<div className="mt-8 flex justify-content-between align-items-center">
						<span className="p-input-icon-left">
							<i className="pi pi-search" />
							<InputText
								placeholder="Digite o nome do usuário"
								value={searchTerm}
								style={{ height: "45px", width: "225px" }}
								onChange={(e) => setSearchTerm(e.target.value)}
							/>
						</span>
						<Button label='Novo úsuario' icon="pi pi-user-plus" className='btn-new-user' onClick={handleCadastro} />
					</div>

					<div className="mt-5 mb-3">
						{!loading && (
							<DataTable value={searchResults} showGridlines tableStyle={{ minWidth: "50rem" }} className="table-users mb-2">
								<Column field="nome" header="Nome"></Column>
								<Column field="email" header="Email"></Column>
								<Column field="grupo" header="Grupo"></Column>
								<Column header="Status" body={(rowData) => (
									<span className={rowData.ativo ? "status-ativo" : "status-inativo"}>
										{rowData.ativo ? "Ativo" : "Inativo"}
									</span>)}>
								</Column>
								<Column header="Ação" body={(rowData) => (
									<InputSwitch checked={rowData.ativo} onChange={() => rowData.ativo ? handleDesativar(rowData) : handleAtivar(rowData)} />)}>
								</Column>
								<Column header="Alterar" body={(rowData) => (
									<Button icon="pi pi-user-edit" className="p-button-info" onClick={() => handleEditUser(rowData)} />
								)} />

							</DataTable>
						)}
						{loading && (
							<DataTable value={items} className="p-datatable-striped">
								<Column header="Nome" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
								<Column header="Email" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
								<Column header="Grupo" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
								<Column header="Status" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
								<Column header="Ação" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
								<Column header="Alterar" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
							</DataTable>
						)}
					</div>
				</div>
			</Card>
			<div className="card paginator">
				<Paginator first={first} rows={rows} totalRecords={totalRecords} onPageChange={onPageChange} />
			</div>

			<ModalEditUser
				userData={userData}
				visible={open}
				setIsClose={handleCloseEditUser}
			/>
		</div>
	);
};

export default BuscarUsuario;
