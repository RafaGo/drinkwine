import { PrimeReactProvider } from 'primereact/api';
import { SessionProvider } from 'next-auth/react';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import '/node_modules/primeflex/primeflex.css'
import 'primeicons/primeicons.css';
import { AppProps } from 'next/app';
import DefaultProvider from '@/contexts/defaultContext';
import { CartProvider } from '@/contexts/cartContext';

function MyApp({ Component, pageProps }) {
  return (
    <PrimeReactProvider>
      <SessionProvider>
        <CartProvider>
          <DefaultProvider>
            <Component {...pageProps} />
          </DefaultProvider>
        </CartProvider>
      </SessionProvider >
    </PrimeReactProvider>
  );
}

export default MyApp;
