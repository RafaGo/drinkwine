import { NextPage } from "next";
import { Card } from "primereact/card";
import { useContext, useEffect, useState } from "react";
import './index.css';
import MeusProdutos from "./meusProdutos";
import HeaderCliente from "@/components/HeaderCliente";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { DefaultContext } from "@/contexts/defaultContext";


const home = () => {

    const router = useRouter();

    const { data: session } = useSession(); 

    console.log("Conteúdo da sessão:", session); 
    console.log(session)


    const formattedNome = session?.token?.nome ? session.token.nome.split(' ')[0].charAt(0).toUpperCase() + session.token.nome.split(' ')[0].slice(1).toLowerCase() : 'Usuário';



    return (
        <main>
            <HeaderCliente userName={formattedNome}></HeaderCliente>
            <section className="banner">
                <img src="images\banner1.png" alt="Drink Wine" className="bannerLogo"/>
                <h1 className="Drink"><strong>D</strong>rink</h1>
                <h1 className="Wine"><strong>W</strong>ine</h1>
            </section>
            <MeusProdutos></MeusProdutos>
        </main>
    );
};



export default home;
