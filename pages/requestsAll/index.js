import { NextPage } from "next";
import { useRouter } from "next/router";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { InputText } from "primereact/inputtext";
import { Paginator } from "primereact/paginator";
import { Skeleton } from "primereact/skeleton";
import { useEffect, useState } from "react";
import '../app.css';
import HeaderEstoquista from "@/components/HeaderEstoquista";
import { useSession } from "next-auth/react";
import { orderBy } from "firebase/firestore";
import OrderDB from "@/public/database/wrappers/order";
import dateFormat from "dateformat";
import ModalEditOrders from "@/components/modals/ModalEditOrder";

const requestsAlls = () => {
  const router = useRouter();
  const [first, setFirst] = useState(0);
  const [rows, setRows] = useState(10);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const [loading, setLoading] = useState(true);
  const [open, setOpen] = useState(false);
  const [orderData, setorderData] = useState({})
  const { data: session } = useSession();

  useEffect(() => {
    async function fetchOrders() {
      try {
        const orders = await new OrderDB().getAll(orderBy('created_at', 'desc'));
        const filteredOrders = orders.filter(order =>
          removeAccents(order.numeroPedido.toLowerCase()).includes(removeAccents(searchTerm.toLowerCase()))
        );
        setTotalRecords(filteredOrders.length);
        const startIndex = currentPage * rows;
        const endIndex = startIndex + rows;
        const ordersForPage = filteredOrders.slice(startIndex, endIndex);
        setSearchResults(ordersForPage);
        setLoading(false);
      } catch (error) {
        console.error('Erro ao pedidos:', error);
      }
    }
    fetchOrders();
  }, [searchTerm, currentPage, rows]);

  const onPageChange = (event) => {
    setFirst(event.first);
    setCurrentPage(event.first / rows);
  };

  const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  };

  const handleCloseEditOrder = () => {
    setOpen(false);
  }


  const handleEditOrder = (order) => {
    setorderData(order);
    setOpen(true);
  }

  const items = Array.from({ length: 10 }, (v, i) => i);
  const formattedNome = session?.token?.nome ? session.token.nome.split(' ')[0].charAt(0).toUpperCase() + session.token.nome.split(' ')[0].slice(1).toLowerCase() : 'Usuário';

  return (
    <div>
      <HeaderEstoquista userName={formattedNome} />
      <Card className="card-list-products h-full">
        <h1 className="mt-7">Lista de Pedidos</h1>

        <div className="mt-8 flex justify-content-between align-items-center">
          <span className="p-input-icon-left">
            <i className="pi pi-search" />
            <InputText
              placeholder="Digite o nome do pedido"
              value={searchTerm}
              style={{ height: '45px' }}
              onChange={(e) => setSearchTerm(e.target.value)}
            />
          </span>
        </div>

        <div className="mt-5 mb-3">
          {!loading && (
            <DataTable value={searchResults} showGridlines tableStyle={{ minWidth: '50rem' }} className="">
              <Column field="created_at" header="Data do Pedido" body={(rowData) => (
                <p className="m-0">{rowData.created_at && dateFormat(rowData.created_at.toDate(), 'dd/mm/yyyy')}</p>
              )}></Column>
              <Column field="numeroPedido" header="N.Pedido"></Column>
              <Column field="valorTotal" header="Valor"></Column>
              <Column field="status" header="Status"></Column>
              <Column header="Alterar" body={(rowData) => (
                <Button icon="pi pi-pencil" className="p-button-info" onClick={() => handleEditOrder(rowData)} />
              )} />

            </DataTable>
          )}
          {loading && (
            <DataTable value={items} className="p-datatable-striped">
              <Column header="Data" style={{ width: '250px', height: '70px' }} body={<Skeleton />}></Column>
              <Column header="Numero" style={{ width: '250px', height: '70px' }} body={<Skeleton />}></Column>
              <Column header="Status" style={{ width: '250px', height: '70px' }} body={<Skeleton />}></Column>
              <Column header="Valor total" style={{ width: '250px', height: '70px' }} body={<Skeleton />}></Column>
              <Column header="Alterar" style={{ width: '250px', height: '70px' }} body={<Skeleton />}></Column>
            </DataTable>
          )}
        </div>

      </Card >
      <div className="card paginator">
        <Paginator first={first} rows={rows} totalRecords={totalRecords} onPageChange={onPageChange} />
      </div>


      <ModalEditOrders
        orderData={orderData}
        visible={open}
        setIsClose={handleCloseEditOrder}
      />

    </div >
  );
};

export default requestsAlls;