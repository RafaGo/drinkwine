import { NextPage } from "next";
import { useRouter } from "next/router";
import { signOut } from "next-auth/react";
import { useSession } from "next-auth/react";
import { Card } from "primereact/card";
import "../app.css";
import "./index.css";
import { useContext, useEffect, useState } from "react";
import HeaderCliente from "@/components/HeaderCliente";
import { Button } from "primereact/button";
import { InputMask } from "primereact/inputmask";
import { RadioButton } from "primereact/radiobutton";
import axios from "axios";
import CartContext from "@/contexts/cartContext";
import AddressDB from "@/public/database/wrappers/address";
import { TIPO_ENDERECO } from "@/types/tipoEndereco";
import Address from "@/public/database/entities/address.entity";
import ModalAdress from "@/components/modals/ModalAdress";
import Payment from "@/components/Payment";
import ResumeOrder from "@/components/ResumeOder";
import { where } from "firebase/firestore";

const Carrinho = () => {
  const { data: session } = useSession();
  const router = useRouter();
  const { cart, valueInCart, handleRemoveItem, decrementProduct, addProductInCart, setFreteValue } = useContext(CartContext);
  const [value, setValue] = useState("");
  const formattedNome = session?.token?.nome ? session.token.nome.split(" ")[0].charAt(0).toUpperCase() + session.token.nome.split(" ")[0].slice(1).toLowerCase() : "Usuário";
  const [logradouro, setLogradouro] = useState("");
  const [cepPesquisado, setCepPesquisado] = useState(false);
  const [modoPagamento, setModoPagamento] = useState(false);
  const [entrega, setEntrega] = useState("");
  const [freteSelecionado, setFreteSelecionado] = useState(0);
  const [enderecoData, setEnderecoData] = useState();
  const [open, setOpen] = useState(false);
  const [addressSelected, setAddressSelected] = useState();
  const [dataEntrega, setDataEntrega] = useState("");
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    if (!session) return;
    const fetchAddress = async () => {
      const address = await new AddressDB(session?.token?.id).getAll(where("padrao", "==", true));
      setAddressSelected(address[0]);
      setValue(address[0].cep);
    };
    fetchAddress();

  }, [session]);

  useEffect(() => {
    if (freteSelecionado > 0) {
      setDataEntrega(calcularDataEntrega(entrega));
      setShowAlert(false);
    }
  }, [freteSelecionado, entrega]);

  const calcularDataEntrega = (tipoFrete) => {
    switch (tipoFrete) {
      case "Expressa":
        return "14/05/2024";
      case "Padrão":
        return "22/05/2024";
      case "Correios":
        return "29/05/2024";
      default:
        return "";
    }
  };

  const buscarLogradouro = async (cep) => {
    try {
      const response = await axios.get(`https://viacep.com.br/ws/${cep}/json/`);
      if (response.data.logradouro) {
        setLogradouro(response.data.logradouro);
        setCepPesquisado(true);
      } else {
        setLogradouro("Endereço não encontrado");
      }
    } catch (error) {
      console.error("Erro ao buscar o logradouro:", error);
    }
  };

  useEffect(() => {
    if (value && !cepPesquisado) {
      buscarLogradouro(value);
    }
  }, [value, cepPesquisado]);

  const frete = freteSelecionado;
  const valorTotal = (valueInCart + freteSelecionado).toFixed(2);

  const handleVoltar = () => {
    router.push("/");
  };

  const handleIrParaPagamento = () => {
    if (session) {
      if (freteSelecionado > 0) {
        setFreteValue(freteSelecionado);
        setModoPagamento(true);
      } else {
        setShowAlert(true);
      }
    } else {
      router.push("/login");
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    if (showAlert) {
      window.scrollTo({ top: 0, behavior: "smooth" });
    }
  }, [showAlert]);
  

  return (
    <div>
      <HeaderCliente />
      <div className="flex justify-content-center" style={{background: "#1E1E1E"}}>
        {showAlert && (
          <div className="message-frete mt-5" role="alert">
            <i className="pi pi-exclamation-triangle" style={{ fontSize: "1.5rem" }}></i>
            <p>Selecione um frete para prosseguir!</p>
          </div>
        )}
      </div>
      {!modoPagamento &&
        <div className="cont-carrinho">
          <h1 className="my-5">Carrinho</h1>
          {cart.length > 0 ? (
            <div className="flex gap-5">
              <div className="cont-itens">
                {cart.map((produto, index) => (
                  <Card key={index} className="card-product">
                    <div className="cont-card">
                      <div className="flex gap-3 align-items-center">
                        <div className="cont-img">
                          <img src={produto.imageMain_url} alt={produto.nome} />
                        </div>
                        <div>
                          <h2 className="m-0">{produto.nome}</h2>
                          <p className="m-0 mt-2">R$ {(produto.preco)}</p>
                        </div>
                      </div>
                      <div className="flex flex-column justify-content-between align-items-end">
                        <i
                          className="pi pi-trash cursor-pointer"
                          style={{ fontSize: "1.5rem", color: "#8A0000" }}
                          onClick={() => handleRemoveItem(index)}
                        ></i>
                        <div key={index} className="flex gap-2 align-items-center cont-counter">
                          <Button icon="pi pi-caret-left" onClick={() => decrementProduct(produto)} disabled={produto.qtd === 1} />
                          <span className="font-bold text-2xl" style={{ width: "25px", display: "flex", justifyContent: "center" }}>{produto.qtd}</span>
                          <Button icon="pi pi-caret-right" onClick={() => addProductInCart(produto)} />
                        </div>
                      </div>
                    </div>
                  </Card>
                ))}
              </div>
              <div className="flex flex-column gap-5">
                <Card className="card-total">
                  <div className="flex gap-2 align-items-center justify-content-center mb-3">
                    <i className="pi pi-chart-bar" style={{ fontSize: "1.5rem" }}></i>
                    <h2 style={{ fontSize: "1.5rem", fontWeight: "300" }}>RESUMO</h2>
                  </div>
                  <div className="flex justify-content-between w-full">
                    <h3>Valor dos Produtos:</h3>
                    <p style={{ color: "#ffa644" }}>R$ {(valueInCart).toFixed(2)}</p>
                  </div>
                  <div className="flex justify-content-between w-full border-top-1">
                    <h3>Frete</h3>
                    <p style={{ color: "#ffa644" }}>R$ {frete.toFixed(2)}</p>
                  </div>
                  <div className="flex justify-content-between w-full border-top-1">
                    <h3>Valor total:</h3>
                    <p style={{ color: "#ffa644" }}>R$ {valorTotal}</p>
                  </div>
                </Card>
                <Card className="card-frete">
                  <div className="flex gap-2 align-items-center justify-content-center mb-">
                    <i className="pi pi-truck" style={{ fontSize: "1.5rem" }}></i>
                    <h2 style={{ fontSize: "1.5rem", fontWeight: "300" }}>ENTREGA</h2>
                  </div>
                  <div className="flex flex-column justify-content-between w-full">
                    {session && (
                      <>
                        <Button
                          label="Escolher outro"
                          icon="pi pi-pencil"
                          onClick={() => setOpen(true)}
                        />
                        {enderecoData && (
                          <ModalAdress
                            enderecoData={enderecoData}
                            visible={open}
                            setIsClose={() => setOpen(false)}
                            setAddressSelected={setAddressSelected}
                            buscarLogradouro={buscarLogradouro}
                          />
                        )}

                        {addressSelected &&
                          <div key={addressSelected?.id}>
                            <p style={{ color: "gray", fontSize: 25 }}>Endereço Principal</p>
                            <p style={{ color: "white", fontSize: 20 }}>{addressSelected.cidade}, </p>
                            <p style={{ color: "white", fontSize: 20 }}> {addressSelected.logradouro}, </p>
                            <p style={{ color: "white", fontSize: 20 }}> {addressSelected.numero} </p>
                            <p style={{ color: "white", fontSize: 20 }}> {addressSelected.complemento} </p>
                          </div>
                        }

                      </>
                    )}
                    <div className="flex justify-content-between align-items-center mb-3">
                      {!session && (
                        <InputMask
                          style={{ height: "35px" }}
                          value={value}
                          onChange={(e) => setValue(e.target.value)}
                          mask="99999-999"
                          placeholder="Informe seu cep"
                        />
                      )}
                      {!session && (
                        <Button style={{ height: "35px", background: "#176fb5", border: "#176fb5" }} label="Buscar" onClick={() => buscarLogradouro(value)} />
                      )}
                    </div>
                  </div>
                  {!session && cepPesquisado && (
                    <div className="flex flex-column justify-content-between w-full border-top-1">
                      <h3>Endereço :</h3>
                      <p style={{ fontSize: "0.9rem", fontWeight: "300", color: "#c3c3c3" }} className="m-0 mb-3">{logradouro}</p>
                    </div>
                  )}
                  {cepPesquisado && (
                    <div className="flex flex-column justify-content-between w-full border-top-1">
                      <h3>Fretes:</h3>
                      <div className="card flex">
                        <div className="flex flex-column gap-3 w-full">
                          <div className="flex align-items-center justify-content-between">
                            <div className="flex align-items-center">
                              <RadioButton
                                inputId="expressa"
                                value="Expressa"
                                onChange={(e) => {
                                  setFreteSelecionado(82.00);
                                  setEntrega(e.value);
                                }}
                                checked={entrega === "Expressa"}
                              />
                              <div className="flex flex-column">
                                <label style={{ fontSize: "1rem", fontWeight: "500" }} htmlFor="expressa" className="ml-2">Expressa</label>
                                <p style={{ fontSize: "0.8rem", fontWeight: "300", color: "#c3c3c3" }} className="ml-2 m-1">Chegará até: 14/05/2024</p>
                              </div>
                            </div>
                            <p style={{ color: "#ffa644" }}>R$ 82,00</p>
                          </div>
                          <div className="flex align-items-center justify-content-between">
                            <div className="flex align-items-center">
                              <RadioButton
                                inputId="Padrão"
                                value="Padrão"
                                onChange={(e) => {
                                  setFreteSelecionado(56.49);
                                  setEntrega(e.value);
                                }}
                                checked={entrega === "Padrão"}
                              />
                              <div className="flex-column">
                                <label style={{ fontSize: "1rem", fontWeight: "500" }} htmlFor="padrão" className="ml-2">Padrão</label>
                                <p style={{ fontSize: "0.8rem", fontWeight: "300", color: "#c3c3c3" }} className="ml-2 m-1">Chegará até: 22/05/2024</p>
                              </div>
                            </div>
                            <p style={{ color: "#ffa644" }}>R$ 56,49</p>
                          </div>
                          <div className="flex align-items-center justify-content-between">
                            <div className="flex align-items-center">
                              <RadioButton
                                inputId="correios"
                                value="Correios"
                                onChange={(e) => {
                                  setFreteSelecionado(47.27);
                                  setEntrega(e.value);
                                }}
                                checked={entrega === "Correios"}
                              />
                              <div className="flex-column">
                                <label style={{ fontSize: "1rem", fontWeight: "500" }} htmlFor="correios" className="ml-2">Correios</label>
                                <p style={{ fontSize: "0.8rem", fontWeight: "300", color: "#c3c3c3" }} className="ml-2 m-1">Chegará até: 29/05/2024</p>
                              </div>
                            </div>
                            <p style={{ color: "#ffa644" }}>R$ 47,27</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </Card>

                <Button
                  label="Ir para Pagamento"
                  onClick={handleIrParaPagamento} />
              </div>
            </div>
          ) : (
            <div className="cont-mensagem">
              <i className="pi pi-box" style={{ fontSize: "2rem" }}></i>
              <p>O Carrinho está vazio</p>
              <Button label="Voltar" onClick={handleVoltar} />
            </div>
          )}
        </div>
      }
      {modoPagamento && (
        <Payment address={addressSelected} products={cart} freteSelecionado={freteSelecionado} tipoFrete={entrega} dataEntrega={dataEntrega} />
      )}
    </div>
  );
};

export default Carrinho;
