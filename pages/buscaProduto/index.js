import ModalEditUser from "@/components/modals/ModalEditUser";
import ProductDB from "@/public/database/wrappers/product";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { InputSwitch } from "primereact/inputswitch";
import { InputText } from "primereact/inputtext";
import { Paginator } from "primereact/paginator";
import { Skeleton } from "primereact/skeleton";
import { useCallback, useEffect, useState } from "react";
import "../app.css";
import "./index.css";
import ModalEditProduct from "@/components/modals/ModalEditProduct";
import ModalViewProduct from "@/components/modals/ModalViewProduct";
import Header from "@/components/Header";
import { orderBy, where } from "firebase/firestore";

const BuscarProduto  = () => {
    const router = useRouter();
    const [first, setFirst] = useState(0);
    const [rows, setRows] = useState(10);
    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState([]);
    const [totalRecords, setTotalRecords] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [loading, setLoading] = useState(true);
    const [open, setOpen] = useState(false);
    const [productData, setproductData] = useState({});
    const { data: session } = useSession(); 
    const [viewProductVisible, setViewProductVisible] = useState(false); 



    useEffect(() => {
		if (!session) {
			router.push("/login");
		}
	}, [session, router]);

	if (!session) {
		return null;
	}





    useEffect(() => {
        async function fetchProduct() {
            try {
                const products = await new ProductDB().getAll(orderBy("created_at", "desc"));
    
                const filteredProducts = products.filter(product =>
                    removeAccents(product.nome.toLowerCase()).includes(removeAccents(searchTerm.toLowerCase()))
                );
                setTotalRecords(filteredProducts.length);
                const startIndex = currentPage * rows;
                const endIndex = startIndex + rows;
                const productsForPage = filteredProducts.slice(startIndex, endIndex);
                setSearchResults(productsForPage);
                setLoading(false);
            } catch (error) {
                console.error("Erro ao buscar produtos:", error);
            }
        }
    
        fetchProduct();
    }, [searchTerm, currentPage, rows]);
    

    const onPageChange = (event) => {
        setFirst(event.first);
        setCurrentPage(event.first / rows);
    };

    const removeAccents = (str) => {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }; 

    const handleCloseEditProduct = () => {
        setOpen(false);
    };


    const handleVisibeProduct = useCallback((product) => {
        setproductData(product);
        setViewProductVisible(true);

    },[]);


    const handleEditProduct = (product) => {
        setproductData(product);
        setOpen(true);
    };

    const handleCadastro = () => {
        router.push("/cadastroProduto");
    };

    const [visible, setVisible] = useState(false);

    const items = Array.from({ length: 10 }, (v, i) => i);

    const handleDesativar = async (product) => {
        const confirmation = window.confirm("Tem certeza de que deseja desativar este produto?");
        if (confirmation) {
            try {
                await new ProductDB().update(product.id, { ...product, ativo: false });

                setSearchResults(prevResults =>
                    prevResults.map(prevUser =>
                        prevUser.id === product.id ? { ...prevUser, ativo: false } : prevUser
                    )
                );
            } catch (error) {
                console.error("Erro ao desativar produto:", error);
            }
        }
    };

    const handleAtivar = async (product) => {
        const confirmation = window.confirm("Tem certeza de que deseja ativar produto?");
        if (confirmation) {
            try {
                await new ProductDB().update(product.id, { ...product, ativo: true });

                setSearchResults(prevResults =>
                    prevResults.map(prevUser =>
                        prevUser.id === product.id ? { ...prevUser, ativo: true } : prevUser
                    )
                );
            } catch (error) {
                console.error("Erro ao ativar produto:", error);
            }
        }
    };

    const formattedNome = session?.token?.nome ? session.token.nome.split(" ")[0].charAt(0).toUpperCase() + session.token.nome.split(" ")[0].slice(1).toLowerCase() : "Usuário";

    return (
        <div>
            <Header userName={formattedNome} />
            <Card className="card-list-products h-full">
                <div className="cont-list-products">
                    <h1 className="mt-7">Lista de Produtos</h1>

                    <div className="mt-8 flex justify-content-between align-items-center">
                        <span className="p-input-icon-left">
                            <i className="pi pi-search" />
                            <InputText
                                placeholder="Digite o nome do produto"
                                value={searchTerm}
                                style={{ height: "45px", width: "230px"}}
                                onChange={(e) => setSearchTerm(e.target.value)}
                            />
                        </span>
                        <Button label='Novo produto' icon="pi pi-plus" className='btn-new-products' onClick={handleCadastro} />
                    </div>

                    <div className="mt-5 mb-3">
                        {!loading && (
                            <DataTable value={searchResults} showGridlines tableStyle={{ minWidth: "50rem" }} className="">
                                <Column field="id" header="Código"></Column>
                                <Column field="nome" header="Nome"></Column>
                                <Column field="qtd" header="Quantidade"></Column>
                                <Column field="preco" header="Preço"></Column>
                                <Column header="Status" body={(rowData) => (
                                    <span className={rowData.ativo ? "status-ativo" : "status-inativo"}>
                                        {rowData.ativo ? "Ativo" : "Inativo"}
                                    </span>)}>
                                </Column>
                                <Column header="Ação" body={(rowData) => (
                                    <InputSwitch checked={rowData.ativo} onChange={() => rowData.ativo ? handleDesativar(rowData) : handleAtivar(rowData)} />)}>
                                </Column>
                                <Column header="Alterar" body={(rowData) => (
                                    <Button icon="pi pi-pencil" className="p-button-info" onClick={() => handleEditProduct(rowData)} />
                                )} />

                                <Column header="Visualizar" body={(rowData) => (
                                    <Button icon="pi pi-eye" className="p-button-info" onClick={() => handleVisibeProduct(rowData)} />
                                )} />
                            </DataTable>
                        )}
                        {loading && (
                            <DataTable value={items} className="p-datatable-striped">
                                <Column header="Código" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                                <Column header="Nome" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                                <Column header="Quantidade" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                                <Column header="Preço" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                                <Column header="Status" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                                <Column header="Ação" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                                <Column header="Alterar" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                                <Column header="Visualizar" style={{ width: "250px", height: "70px" }} body={<Skeleton />}></Column>
                            </DataTable>
                        )}
                    </div>
                </div>
            </Card>
            <div className="card paginator">
                <Paginator first={first} rows={rows} totalRecords={totalRecords} onPageChange={onPageChange} />
            </div> 

            <ModalEditProduct 
                productData={productData}
                visible={open}
                setIsClose={handleCloseEditProduct}
            />

            <ModalViewProduct
                visible={viewProductVisible}
                setIsClose={() => setViewProductVisible(false)}
                productDataView={productData}
            />

        </div>
    );
};

export default BuscarProduto;