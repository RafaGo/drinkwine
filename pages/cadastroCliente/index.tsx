import { NextPage } from "next";
import AddressDB from "@/public/database/wrappers/address";
import Client from "@/public/database/entities/client.entity";
import ClientDB from "@/public/database/wrappers/client";
import { useCallback, useMemo, useState } from "react";
import { Card } from "primereact/card";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import apiViacep from "@/public/services/apiViaCep";
import { ErrorMessage, Field, FieldArray, FieldProps, Form, Formik } from "formik";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "@/public/database/firebase";
import { hashSync } from "bcryptjs";
import { InputMask } from "primereact/inputmask";
import { Password } from "primereact/password";
import { TIPO_ENDERECO } from "@/types/tipoEndereco";
import router from "next/router";
import "./index.css";
import "../../pages/app.css";

const CadastroCliente: NextPage = () => {
	const [errorMessage, setErrorMessage] = useState("");
	const [loading, setLoading] = useState(false);


	function TestaCPF(strCPF: any) {
		var Soma;
		var Resto;
		Soma = 0;
		strCPF = strCPF.replace(/[^0-9]/g, "");
		if (strCPF == "00000000000") return false;

		for (var i = 1; i <= 9; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;

		if ((Resto == 10) || (Resto == 11)) Resto = 0;
		if (Resto != parseInt(strCPF.substring(9, 10))) return false;

		Soma = 0;
		for (var i = 1; i <= 10; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;

		if ((Resto == 10) || (Resto == 11)) Resto = 0;
		if (Resto != parseInt(strCPF.substring(10, 11))) return false;
		return true;
	}
	const validate = async (values: any) => {

		values.cpf = values.cpf.replace(/[^0-9]/g, "");
		let cpfMask;
		cpfMask = values.cpf.match(/(\d{3})(\d{3})(\d{3})(\d{2})/);
		if (cpfMask !== null) values.cpf = `${cpfMask[1]}.${cpfMask[2]}.${cpfMask[3]}-${cpfMask[4]}`;
		const errors: any = {};

		if (!values.nome) {
			errors.nome = "Este campo é necessário";
		} else if (values.nome.length < 3) {
			errors.nome = "O nome precisa ter 3 caracteres ou mais";
		}
		if (values.senha !== values.confirmarSenha) {
			errors.confirmarSenha = "As senhas não coincidem";
		}
		if (!values.cpf) {
			errors.cpf = "Este campo é necessário";
		} else if (cpfMask == null) {
			errors.cpf = "Informe o CPF corretamente";
		} else if (TestaCPF(values.cpf) == false) errors.cpf = "Informe um CPF válido";
		if (!values.nascimento) {
			errors.ativo = "Você precisa escolher o status do usuário";
		}
		if (!values.senha) {
			errors.senha = "Este campo é necessário";
		} else if (values.senha.length < 6) {
			errors.senha = "A senha precisa ter 6 caracteres";
		}
		if (!values.sexo) {
			errors.grupo = "Este campo é necessário";
		}
		if (!values.email) {
			errors.email = "Este campo é necessário.";
		}

		return errors;
	};
	const getAddress = useCallback(async (cep: string) => {
		try {
			const response = await apiViacep(`${cep}/json`);
			return response.data;
		} catch (error) {
			console.error("Erro ao obter endereço:", error);
			return null;
		}
	}, []);


	return (
		<div className="cont">
			<Card className="card-cadastro">
				<h1>Cadastre-se</h1>
				<Formik
					initialValues={{
						nome: "",
						nascimento: "",
						email: "",
						cpf: "",
						sexo: "",
						senha: "",
						confirmarSenha: "",
						cep: "",
						ativo: true,
						complemento: "",
						numero: "",
						cidade: "",
						uf: "",
						bairro: "",
						logradouro: "",
					}}
					validate={validate}
					onSubmit={async (values, { setErrors, setSubmitting, resetForm }) => {
						setLoading(true);
						const unmaskCpf = values.cpf?.replace(/\D/g, "");
						try {
							const { nome, nascimento, email, senha, sexo, cep } = values;
							const password = senha;
							const senhaHash = hashSync(senha, 10);
							const newClient: Client = {
								nome: nome,
								cpf: unmaskCpf,
								email: email,
								senha: senhaHash,
								nascimento: nascimento,
								sexo: sexo,
							};
							const clientCredential = await createUserWithEmailAndPassword(
								auth,
								email,
								password,
							);
							const client = clientCredential.user;
							console.log(client);
							if (client) {
								const createClient = await new ClientDB().createWithCustomId(client.uid, newClient);
								await new AddressDB(client.uid).create({
									cep: values.cep,
									cidade: values.cidade,
									bairro: values.bairro,
									numero: values.numero,
									complemento: values.complemento,
									logradouro: values.logradouro,
									uf: values.uf,
									ativo: true,
									faturamento: true,
									tipoEndereco: TIPO_ENDERECO.entrega,
									padrao: true // 
								});
								console.log("Cliente e endereço cadastrados com sucesso!");
								router.push("/login");
							}
						} catch (error: any) {
							console.error("Erro ao cadastrar usuário:", error);
							if (error.code === "auth/email-already-in-use") {
								setErrorMessage("Este e-mail já está sendo usado.");
								setErrors({ email: "Este e-mail já está sendo usado." });
							} else {
								setErrorMessage("Ocorreu um erro ao cadastrar o usuário.");
								setErrors({});
							}
						}
						setSubmitting(false);
						setLoading(false);
					}}
				>
					{({ values, handleSubmit, handleChange, setFieldValue }) => (
						<Form onSubmit={handleSubmit}>
							<div className="flex flex-column justify-content-center align-items-center gap-4 mt-7">
								<div className="flex flex-wrap gap-4">
									<div className="flex flex-column gap-2">
										<label htmlFor="nome" className="form-label">Nome</label>
										<Field type="nome" name="nome" render={({ field }: FieldProps<any>) => <InputText {...field} id="nome" />} />
										<ErrorMessage name="nome" component="div" className="text-danger" />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="email" className="form-label">Email</label>
										<Field type="email" name="email" render={({ field }: FieldProps<any>) => <InputText {...field} id="email" />} />
										<ErrorMessage name="email" component="div" className="text-danger" />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="senha" className="form-label">Senha</label>
										<Field type="password" name="senha" render={({ field }: FieldProps<any>) => <Password {...field} id="senha" toggleMask feedback={false} />} />
										<ErrorMessage name="senha" component="div" className="text-danger" />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="confirmarSenha" className="form-label">Confirmar Senha</label>
										<Field type="password" name="confirmarSenha" render={({ field }: FieldProps<any>) => <Password {...field} id="confirmarSenha" toggleMask feedback={false} />} />
										<ErrorMessage name="confirmarSenha" component="div" className="text-danger" />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="cpf" className="form-label">CPF</label>
										<Field type="cpf" name="cpf" render={({ field }: FieldProps<any>) => <InputMask mask="999.999.999-99" {...field} id="cpf" />} />
										<ErrorMessage name="cpf" component="div" className="text-danger" />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="nascimento">Nascimento</label>
										<InputText id="nascimento" name="nascimento" value={values.nascimento} onChange={handleChange} />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="sexo">Sexo</label>
										<InputText id="sexo" name="sexo" value={values.sexo} onChange={handleChange} />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="cep">CEP</label>
										<InputMask
											mask="99999-999"
											id="cep"
											name="cep"
											value={values.cep}
											onChange={handleChange}
											onBlur={async (e) => {
												const cep = e.target.value;
												const addressData = await getAddress(cep);
												if (addressData) {
													handleChange(e);
													setFieldValue("cidade", addressData.localidade);
													setFieldValue("uf", addressData.uf);
													setFieldValue("bairro", addressData.bairro);
													setFieldValue("logradouro", addressData.logradouro);
												}
											}}
										/>
										<ErrorMessage name="cep" component="div" className="text-danger" />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="cidade">Cidade</label>
										<InputText id="cidade" name="cidade" value={values.cidade} onChange={handleChange} />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="uf">UF</label>
										<InputText id="uf" name="uf" value={values.uf} onChange={handleChange} />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="bairro">Bairro</label>
										<InputText id="bairro" name="bairro" value={values.bairro} onChange={handleChange} />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="logradouro">Logradouro</label>
										<InputText id="logradouro" name="logradouro" value={values.logradouro} onChange={handleChange} />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="numero">Número</label>
										<InputText id="numero" name="numero" value={values.numero} onChange={handleChange} />
									</div>

									<div className="flex flex-column gap-2">
										<label htmlFor="complemento">Complemento</label>
										<InputText id="complemento" name="complemento" value={values.complemento} onChange={handleChange} />
									</div>
								</div>
							</div>
							<div className="mt-5 cont-btn">
								<Button
									label="Cadastrar"
									disabled={loading}
									className="btn-cadastrar"
									loading={loading}
									iconPos="right"
									type="submit"
								/>
							</div>
						</Form>
					)}
				</Formik>
			</Card>
		</div >
	);
};

export default CadastroCliente;