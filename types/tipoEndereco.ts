export const TIPO_ENDERECO = {
    entrega: 'entrega',
    faturamento: 'faturamento',
    padrao: 'padrao',
}


export const TIPO_ENDERECO_PT_BR = {
    [TIPO_ENDERECO.entrega]: 'entrega',
    [TIPO_ENDERECO.faturamento]: 'faturamento',
    [TIPO_ENDERECO.padrao]: 'padrao',

}