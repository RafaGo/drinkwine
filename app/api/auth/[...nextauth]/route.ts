import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "@/public/database/firebase";
import UserDB from "@/public/database/wrappers/user";
import ClientDB from "@/public/database/wrappers/client";

const { admin } = require('../../../../public/database/admin');

const authAdmin = admin.auth();

interface Credentials {
	email: string;
	password: string;
}

interface FirebaseUser {
	uid: string;
	displayName: string | null;
	email: string | null;
	photoURL: string | null;
}

function convertFirebaseUserToNextAuthUser(firebaseUser: FirebaseUser) {
	const nextAuthUser = {
		id: firebaseUser.uid,
		name: firebaseUser.displayName,
		email: firebaseUser.email,
		photoURL: firebaseUser.photoURL
	};
	return nextAuthUser;
}

const handler = NextAuth({
	providers: [
		CredentialsProvider({
			name: "Credentials",
			credentials: {
				email: { label: "email", type: "text" },
				password: { label: "password", type: "password" },
			},
			async authorize(credentials, req) {
				const { email, password } = credentials as Credentials
				try {
					const { user } = await signInWithEmailAndPassword(auth, email, password);
					if (!user) return null;
					const nextAuthUser = convertFirebaseUserToNextAuthUser(user);
					return nextAuthUser;
				} catch (error) {
					console.error(error);
					return null;
				}
			}
		})
	],


	callbacks: {
		async signIn({ user, account, profile }: any) {
			try {
				const userRecord = await authAdmin.getUserByEmail(user.email);
				if (userRecord) {
					// console.log('Usuário já existe no Firebase Authentication');
					return true;
				} else {
					return false;
				}
			} catch (error) {
				console.error('Ocorreu um erro ao verificar ou criar a conta:', error);
				return false;
			}
		},
		async session({ session, token, user }: any) {
			session.token = token;

			return session
		},
		async jwt({ token, user, profile, account }: any) {
			try {
				if (user) {
					const clientDoc = await new ClientDB().get(user.id).catch(error => {
						console.error('Erro ao obter clientDoc:', error);
						return null; 
					});
					const userDoc = await new UserDB().get(user.id).catch(error => {
						console.error('Erro ao obter userDoc:', error);
						return null; 
					});
	
					if (clientDoc && !userDoc) {
						console.log('CAIU CONDIÇÃO CLIENTE');
						token.userDoc = clientDoc;
						user = clientDoc;
					}

					if (userDoc && !clientDoc) {
						console.log('CAIU CONDIÇÃO USER');
						token.user = userDoc;
						user = userDoc;
					}

					token = { ...token, ...user };
				}
			} catch (error) {
				console.error('Erro inesperado ao processar o JWT:', error);
			}

			return token;
		}



	},
	pages: {
		signIn: "/login",
		error: "/",
		// error: "/auth-pages/sign-up",
	},

})



export { handler as GET, handler as POST }
