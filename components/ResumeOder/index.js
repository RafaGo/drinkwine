import React, { useCallback, useContext, useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import CartContext from "@/contexts/cartContext";
import { Button } from 'primereact/button';
import './style.css';
import Order from "@/public/database/entities/order.entity";
import { useSession } from "next-auth/react";
import OrderDB from "@/public/database/wrappers/order";
import router, { Router } from "next/router";

const ResumeOrder = ({ address, products, parcelas, valorParcelado, metodoPagamento, freteSelecionado, tipoFrete, dataEntrega, onReturnToPayment }) => {
  const { valueInCart, freteValue, totalComFrete, clearCart } = useContext(CartContext);
  const { data: session } = useSession();
  const [modeRequests, setModeRequests] = useState(false);
  const [resumeOrderVisible, setResumeOrderVisible] = useState(true);
  const [loading, setLoading] = useState(false);

  const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };


  const handleHome = () => {
    router.push('/');
  };

  const totalValueOfProducts = products.reduce((total, produto) => {
    return total + parseFloat(produto.preco) * produto.qtd;
  }, 0);

  const totalPurchaseValue = totalValueOfProducts + parseFloat(freteSelecionado);
  const handleRequests = () => {
    setModeRequests(true);
    setResumeOrderVisible(false);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const generateOrderNumber = async () => {
    setLoading(true);

    const orders = await new OrderDB().getAll();
    const requestNumber = orders.length;
    setLoading(false)
    return `P0${requestNumber + 1}`
  }

  const finishPayment = useCallback(async () => {
    try {
      const requestNumber = await generateOrderNumber();
      const data = {
        address,
        clientId: session?.token?.id,
        products,
        status : 'aguardando pagamento ',
        paid: false,
        metodoPagamento,
        valorCarrinho: valueInCart,
        valorFrete: freteValue,
        valorTotal: totalComFrete,
        numeroPedido: requestNumber,
      };

      await new OrderDB().create(data);
      window.alert(`Nº Pedido: ${requestNumber}`);
      clearCart()
      handleHome();
    } catch (error) {
      console.error('Erro ao finalizar pagamento:', error);
    }
  }, [session])

  return (
    <div>
      {resumeOrderVisible && (
        <Card className="cont-resume">
          <div className="flex gap-3 mt-5">
            <div className="flex flex-column gap-3">
              <h1 className="m-0 mt-4">Produtos:</h1>
              {products.map((produto, index) => (
                <Card key={index} className="products">
                  <div className="flex gap-2 align-items-center">
                    <div className="cont-img">
                      <img src={produto.imageMain_url} alt={produto.nome} />
                    </div>
                    <div>
                      <h2 className="m-0">{produto.nome}</h2>
                      <p className="m-0 mt-2 flex align-items-center gap-1">Quantidade: <p className="quant">{produto.qtd}</p></p>
                      <p className="m-0 mt-2 flex align-items-center gap-1">Valor unitário: <p className="quant">{produto.preco}</p></p>

                    </div>
                  </div>

                  <div>
                    <p className="m-0 mt-2">R$ {(parseFloat(produto.preco) * produto.qtd).toFixed(2)}</p>
                  </div>
                </Card>
              ))}
            </div>
            <div className="flex flex-column gap-3">
              <div className="value mt-8 flex flex-column justify-content-between">
                <div>
                  <h1>Valor da compra</h1>
                  <div className="flex justify-content-between align-items-center mt-6">
                    <h2>Valor dos Produtos:</h2>
                    <p>R$ {totalValueOfProducts.toFixed(2)}</p>
                  </div>
                  <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                    <h2>Frete:</h2>
                    <p>{Number.isInteger(freteSelecionado) ? `${freteSelecionado}.00` : freteSelecionado}</p>
                  </div>
                  <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                    <h2>Total da compra:</h2>
                    <p>R$ {totalPurchaseValue.toFixed(2)}</p>
                  </div>
                  {metodoPagamento === "cartao" && (
                    <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                      <h2>Total a prazo</h2>
                      <div className="flex flex-column align-items-end">
                        <p>R$ {valorParcelado}</p>
                        <label>(em {parcelas}x de R$ {(valueInCart / parcelas).toFixed(2)} sem juros)</label>
                      </div>
                    </div>
                  )}
                  <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                    <h2>Método de pagamento:</h2>
                    <p className="metodo-pagamento">{capitalizeFirstLetter(metodoPagamento)}</p>
                  </div>
                </div>
              </div>
              <div className="delivery">
                <h1>Endereço de entrega</h1>
                <div className="flex justify-content-between align-items-center mt-6">
                  <h2>Rua:</h2>
                  <p>{address.logradouro}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                  <h2>Número:</h2>
                  <p>{address.numero}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                  <h2>Complemento:</h2>
                  <p>{address.complemento}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                  <h2>Bairro:</h2>
                  <p>{address.bairro}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                  <h2>CEP:</h2>
                  <p>{address.cep}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                  <h2>Cidade:</h2>
                  <p>{address.cidade}</p>
                </div>
              </div>
              <div className="frete">
                <h1>Frete</h1>
                <div className="flex justify-content-between align-items-center mt-6">
                  <h2>Tipo:</h2>
                  <p>{tipoFrete}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                  <h2>Data de entrega:</h2>
                  <p>{dataEntrega}</p>
                </div>
                <div className="flex justify-content-between align-items-center mt-1 border-top-1">
                  <h2>Valor do frete:</h2>
                  <p>{Number.isInteger(freteSelecionado) ? `${freteSelecionado}.00` : freteSelecionado}</p>
                </div>
              </div>
              <div className="card flex justify-content-between">
                <Button label="Voltar" onClick={onReturnToPayment} />
                <Button label="Finalizar Pagamento" severity="success" onClick={finishPayment} />
              </div>
            </div>
          </div>
        </Card>
      )}
    </div>
  );
};

export default ResumeOrder;
