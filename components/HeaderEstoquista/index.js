import { signOut } from "next-auth/react";
import router from "next/router";
import { Avatar } from "primereact/avatar";
import { Menu } from "primereact/menu";
import { useRef, useState } from "react";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { Button } from "primereact/button";
import "./index.css";

const HeaderEstoquista = ({ userName }) => {

    const [userInitial, setUserInitial] = useState(userName ? userName.charAt(0).toUpperCase() : "");

    const handleSignOut = () => {
        signOut({ callbackUrl: "/login" });
    };

    const handleBuscaProduto = () => {
        router.push("/produtoEstoquista");
    };


    const handleRequests = () => {
        router.push("/requestsAll");
    };


    const handleBackoffice = () => {
        router.push("/backofficeEstoquista");
    };

    const accept = () => {
        handleSignOut();
        router.push("/");
    };

    const reject = () => {
    };

    const confirm = () => {
        confirmDialog({
            group: "headless",
            message: "Tem certeza que deseja sair?",
            header: "Confirmação",
            icon: "pi pi-exclamation-triangle",
            defaultFocus: "accept",
            accept,
            reject
        });
    };


    const menuLeft = useRef(null);
    const menuRight = useRef(null);
    const toast = useRef(null);
    const items = [
        {
            label: `Olá ${userName}`,
            items: [
                {
                    label: "Sair",
                    icon: "pi pi-sign-out",
                    command: confirm
                },
            ]
        }
    ];

    return (
        <div className='header-estoquista'>
            <img src="images\logo.png" alt="teste" />

            <ul className='flex gap-4'>
                <li className='flex align-items-center cursor-pointer' onClick={handleBackoffice}>
                    <i className="pi pi-briefcase mr-2 cursor-pointer" style={{ fontSize: "1.2rem" }}></i>
                    <label htmlFor="backoffice" className='cursor-pointer'>Backoffice</label>
                </li>
                <li className='flex align-items-center cursor-pointer' onClick={handleBuscaProduto}>
                    <i className="pi pi-users mr-2 cursor-pointer" style={{ fontSize: "1.2rem" }}></i>
                    <label htmlFor="Lista de úsuarios" className='cursor-pointer'>Produtos</label>
                </li>
                <li className='flex align-items-center cursor-pointer' onClick={handleRequests}>
                    <i className="pi pi-users mr-2 cursor-pointer" style={{ fontSize: "1.2rem" }}></i>
                    <label htmlFor="Lista de úsuarios" className='cursor-pointer'>Pedidos</label>
                </li>
            </ul>

            <div className="card flex justify-content-center">
                <Menu model={items} popup ref={menuRight} id="popup_menu_right" popupAlignment="right" className='mt-2' />
                <Avatar label={userInitial} style={{ backgroundColor: "#9c27b0", color: "#ffffff" }} shape="circle"
                    onClick={(event) => menuRight.current.toggle(event)} />
            </div>
            <ConfirmDialog
                group="headless"
                content={({ headerRef, contentRef, footerRef, hide, message }) => (
                    <div className="flex flex-column align-items-center p-5 surface-overlay border-round">
                        <div className="flex gap-2 align-items-center mb-3">
                            <i className="pi pi-question-circle text-3xl" style={{ color: "#440C0C" }}></i>
                            <p className="m-0 message-dialog" ref={contentRef}>
                                {message.message}
                            </p>
                        </div>
                        <div className="flex align-items-center gap-2 mt-4" ref={footerRef}>
                            <Button
                                label="Sim"
                                onClick={(event) => {
                                    hide(event);
                                    accept();
                                }}
                                className="w-8rem"
                                style={{ background: "#440C0C", borderColor: "#440C0C", color: "#fff" }}
                            ></Button>
                            <Button
                                label="Não"
                                outlined
                                onClick={(event) => {
                                    hide(event);
                                    reject();
                                }}
                                className="w-8rem"
                                style={{ borderColor: "#440C0C", color: "#440C0C" }}
                            ></Button>
                        </div>
                    </div>
                )}
            />
        </div>
    );
};

export default HeaderEstoquista;
