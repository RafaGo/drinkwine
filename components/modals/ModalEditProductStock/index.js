import { DefaultContext } from "@/contexts/defaultContext";
import ProductDB from "@/public/database/wrappers/product";
import { FormikValues, useFormik } from "formik";
import router from "next/router";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { useCallback, useContext, useEffect } from "react";



const ModalEditProductStock  = ({ visible, setIsClose, productData }) => {
    const { product,user } = useContext(DefaultContext);

    useEffect(() => {
        if (!visible) return formik.resetForm();

        if (productData) {
            const { nome, avaliacao, descricao, preco, qtd } = productData;
            formik.setValues({
                nome,
                avaliacao,
                descricao,
                preco,
                qtd,
            });
        }
    }, [productData, visible]);

    const formik = useFormik({
        initialValues: {
            nome: '',
            avaliacao: '',
            descricao: '',
            preco: '',
            qtd: '',
        },
        onSubmit: async (values) => {
            try {
                const productDB = new ProductDB();
                await productDB.update(productData.id, values);
                console.log('Dados do produto atualizados com sucesso.');
                router.push('/backofficeEstoquista');
            } catch (error) {
                console.error('Erro ao salvar os dados do usuário:', error);
            }
        }
    }); 



    return (
        <Dialog header="Editar produto" className='modal-edit' visible={visible} style={{ width: '50vw' }} onHide={setIsClose}>
            <div className="p-fluid p-grid p-formgrid">
                <div className="p-field p-col-12">
                    <label htmlFor="nome">Nome</label>
                    <InputText id="nome" disabled={visible} {...formik.getFieldProps('nome')} />

                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="avaliacao">Avaliação</label>
                    <InputText id="avaliacao"  disabled={visible}{...formik.getFieldProps('avaliacao')} />
                </div>

                <div className="p-field p-col-12">
                    <label htmlFor="preco">Quantidade</label>
                    <InputText id="qtd"  {...formik.getFieldProps('qtd')} />
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="preco">Preço</label>
                    <InputText id="preco"  disabled={visible}{...formik.getFieldProps('preco')} />
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="descricao">Descrição</label>
                    <InputText id="descricao" disabled={visible} {...formik.getFieldProps('descricao')} />
                </div>
                <div className="p-field p-col-12">
                    <Button label="Salvar" onClick={() => formik.handleSubmit()} />
                </div>
            </div>
        </Dialog>
    );
};

export default ModalEditProductStock;
