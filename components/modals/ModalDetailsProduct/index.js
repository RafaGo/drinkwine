import { DefaultContext } from "@/contexts/defaultContext";
import ProductDB from "@/public/database/wrappers/product";
import { FormikValues, useFormik } from "formik";
import router from "next/router";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { useCallback, useContext, useEffect } from "react";
import "../../../pages/app.css";
import "./index.css";
import { Rating } from "primereact/rating";
import { Carousel } from "primereact/carousel";
import HeaderCliente from "@/components/HeaderCliente";
import { useSession } from "next-auth/react";
import CartContext, { CartProvider } from "@/contexts/cartContext";

const productTemplate = (item) => {
	return (
		<div className="product-item">
			<img src={item} alt="Product" />
		</div>
	);
};


const ModalDetailsProduct = ({ visible, setIsClose, productDataView }) => {
	const {addProductInCart} = useContext(CartContext);

	useEffect(() => {
		if (!visible) return formik.resetForm();

		if (productDataView) {
			const { nome, avaliacao, descricao, preco, image_url, image, imageMain, imageMain_url } = productDataView;
			formik.setValues({
				nome,
				avaliacao,
				descricao,
				preco,
				image_url,
				image,
				imageMain,
				imageMain_url,
			});
		}
	}, [productDataView, visible]);

	const formik = useFormik({
		initialValues: {
			nome: "",
			avaliacao: "",
			descricao: "",
			preco: "",
			image: [],
			image_url: [],
			imageMain: [],
			imageMain_url: []
		},
		onSubmit: async (values) => {
			try {
				const productDB = new ProductDB();
				await productDB.update(productDataView.id, values);
				console.log("Dados do produto atualizados com sucesso.");
				router.push("/backoffice");
			} catch (error) {
				console.error("Erro ao salvar os dados do usuário:", error);
			}
		}
	});

	const handleMainImage = useCallback((e) => {
		const [file] = Array.from(e.target.files);
		formik.setValues({
			...formik.values,
			imageMain: file,
			imageMain_url: URL.createObjectURL(file)
		});
	}, [formik.values]);

	const handleOtherImages = useCallback((e) => {
		if (e.target && e.target.files) {
			const files = Array.from(e.target.files);
			const newImageUrls = files.map((file) => URL.createObjectURL(file));

			let updatedImages;
			if (Array.isArray(formik.values.image)) {
				updatedImages = [...formik.values.image, ...files];
			} else {
				updatedImages = [...files];
			}

			formik.setValues({
				...formik.values,
				image: updatedImages,
				image_url: [...formik.values.image_url, ...newImageUrls],
			});
		}
	}, [formik.values]);

	const handleDeleteOtherImage = useCallback((index) => {
		const updatedImages = Array.isArray(formik.values.image) ? [...formik.values.image] : [];
		updatedImages.splice(index, 1);

		const updatedImageUrls = Array.isArray(formik.values.image_url) ? [...formik.values.image_url] : [];
		updatedImageUrls.splice(index, 1);

		formik.setValues({
			...(formik.values),
			image: updatedImages,
			image_url: updatedImageUrls,
		});
	}, [formik.values]); 

	const goCart = () => {
		router.push("/carrinho"); 
}; 

const handleBuyClick = (productDataView) => {
	addProductInCart(productDataView); 
	goCart(); 
};

	const { data: session } = useSession();

	const formattedNome = session?.token?.nome ? session.token.nome.split(" ")[0].charAt(0).toUpperCase() + session.token.nome.split(" ")[0].slice(1).toLowerCase() : "Usuário";
	return (
		<Dialog className='view-a-product' visible={visible} onHide={setIsClose} headerClassName="headerProduct">
			<HeaderCliente userName={formattedNome}></HeaderCliente>
			<div className="pt-3" style={{ background: "#1E1E1E" }}>
				<i className="pi pi-arrow-circle-left cursor-pointer pl-5" style={{ fontSize: "2rem", color: "#fff" }} onClick={setIsClose}></i>
				<div className="cont-product">
					<div className="product">
						<div className=" flex align-items-center justify-content-between mb-5">
							<div className="p-field p-col-12">
								{formik.values.imageMain_url && (
									<img src={formik.values.imageMain_url} alt="Imagem principal" />
								)}
							</div>
							<div className="flex flex-column align-items-center ml-7">
								<label className="price">R${formik.values.preco}</label>
								<Button onClick={() =>  handleBuyClick(productDataView)} label="Comprar" className="btn-comprar mt-5" />

								<Button onClick={() => addProductInCart(productDataView)} label="Adicionar ao carrinho" className="btn-adicionar mt-5" />
							</div>
						</div>
						<div>
							<label>{formik.values.nome}</label>
							<div className="mt-5">
								<h2 className="mb-2">Avaliação:</h2>
								<Rating value={formik.values.avaliacao} readOnly cancel={false} />
							</div>
							<div className="mt-5">
								<h2 className="mb-2">Descrição:</h2>
								<p>{formik.values.descricao}</p>
							</div>
						</div>
						<div className="mt-5">
							<Carousel
								value={formik.values.image_url}
								numVisible={1}
								showIndicators={false}
								numScroll={1}
								itemTemplate={productTemplate}
							/>
						</div>
					</div>
				</div>
			</div>
		</Dialog>
	);
};

export default ModalDetailsProduct;
