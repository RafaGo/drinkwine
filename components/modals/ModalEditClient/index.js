import { Dialog } from "primereact/dialog";
import { useFormik } from "formik";
import { useSession } from "next-auth/react";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { useEffect, useState } from "react";
import ClientDB from "@/public/database/wrappers/client";
import { compareSync, hashSync } from "bcryptjs";
import { Password } from "primereact/password";


const ModalEditClient = ({ visible, setIsClose }) => {
  const { data: session  } = useSession();
  const [clientData, setClientData] = useState(null); 
  const clientDB = new ClientDB();

  useEffect(() => {
    const fetchClientData = async () => {
      try {
        if (session) {
          const data = await clientDB.get(session?.token?.id);
          setClientData(data);
        }
      } catch (error) {
        console.error('Erro ao buscar os dados do cliente:', error);
      }
    };

    if (visible && session) {
      fetchClientData();
    }
  }, [visible, session]);

  const validate = async (values) => {
    const errors = {};

    if (!values.nome) {
      errors.nome = 'Este campo é necessário';
    } else if (values.nome.length < 3) {
      errors.nome = 'O nome precisa ter 3 caracteres ou mais';
    }
    if (values.senha !== values.confirmarSenha) {
      errors.confirmarSenha = 'As senhas não coincidem';
    }
    if (!values.senha) {
      errors.senha = 'Este campo é necessário';
    } else if (values.senha.length < 6) {
      errors.senha = 'A senha precisa ter 6 caracteres';
    }

    if (!clientData || !compareSync(values.senhaAtual, clientData.senha)) {
      errors.senhaAtual = 'Senha atual incorreta';
    }

    return errors;
  };

  const formik = useFormik({
    initialValues: {
      email: clientData?.token?.email || "",
      nome: clientData?.nome || "",
      nascimento: clientData?.nascimento || "",
      sexo: clientData?.sexo || "",
      senha: '',
      confirmarSenha: '',
      senhaAtual: '',
    },
    validate,
    onSubmit: async (values) => {
      const senhaHash = hashSync(values.senha, 10);
      const updatedUserData = { ...values, senha: senhaHash };
      const { confirmarSenha, senhaAtual, ...userDataWithoutConfirmarSenha } = updatedUserData;
      try {
        await clientDB.update(session?.token?.id, userDataWithoutConfirmarSenha);
        console.log('Dados do cliente atualizados com sucesso.');
        setIsClose();
      } catch (error) {
        console.error('Erro ao salvar os dados do cliente:', error);
      }
    }
  });

  useEffect(() => {
    formik.setValues({
      ...formik.values,
      nome: clientData?.nome || "",
      email: clientData?.email || "",
      nascimento: clientData?.nascimento || "",
      sexo: clientData?.sexo || "",
    });
  }, [clientData]);

  useEffect(() => {
    if (!visible) {
      formik.resetForm();
    }
  }, [visible])

  return (
    <Dialog header="Editar cliente" className='modal-edit' visible={visible} style={{ width: '50vw' }} onHide={setIsClose}>
      <form onSubmit={formik.handleSubmit}>
        <div className="p-fluid p-grid p-formgrid">
          <div className="p-field p-col-12">
            <label htmlFor="nome">Nome</label>
            <InputText id="nome" {...formik.getFieldProps('nome')} />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="email">Email</label>
            <InputText id="email" value={formik.values.email} disabled />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="sexo">Sexo</label>
            <InputText id="sexo" {...formik.getFieldProps('sexo')} />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="nascimento">Nascimento</label>
            <InputText id="nascimento" {...formik.getFieldProps('nascimento')} />
          </div>
          <div className="flex flex-column gap-2">
            <label htmlFor="senhaAtual">Senha atual</label>
            <Password
              id="senhaAtual"
              value={formik.values.senhaAtual}
              onChange={(e) => formik.setFieldValue('senhaAtual', e.target.value)}
              feedback={false}
              toggleMask
            />
            <p>{formik.errors.senhaAtual}</p>
          </div>

          <div className="flex flex-column gap-2">
            <label htmlFor="senha">Nova senha</label>
            <Password
              id="senha"
              value={formik.values.senha}
              onChange={(e) => formik.setFieldValue('senha', e.target.value)}
              feedback={false}
              toggleMask
            />
            <p>{formik.errors.senha}</p>
          </div>

          <div className="flex flex-column gap-2">
            <label htmlFor="confirmarSenha">Confirmar senha</label>
            <Password
              id="confirmarSenha"
              value={formik.values.confirmarSenha}
              onChange={(e) => formik.setFieldValue('confirmarSenha', e.target.value)}
              feedback={false}
              toggleMask
            />
            <p>{formik.errors.confirmarSenha}</p>
          </div>
        </div>

        <div className="p-field p-col-12">
          <Button label="Editar" type="submit" />
        </div>
      </form>
    </Dialog>
  );
};

export default ModalEditClient;
