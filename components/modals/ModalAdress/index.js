import Address from "@/public/database/entities/address.entity";
import AddressDB from "@/public/database/wrappers/address";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { useCallback, useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { TIPO_ENDERECO } from "@/types/tipoEndereco";
import { FieldArray, Form, Formik } from "formik";
import { InputText } from "primereact/inputtext";
import { InputMask } from "primereact/inputmask";
import apiViacep from "@/public/services/apiViaCep";



const ModalAdress = ({ visible, setIsClose, buscarLogradouro, setAddressSelected }) => {
  const { data: session } = useSession();
  const [addresses, setAddresses] = useState<[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (session) {
      const onSubscriber = new AddressDB(session.token.id).on(end => {
        setAddresses(end.filter((end) => end.tipoEndereco === TIPO_ENDERECO.entrega));
      });
      return () => {
        onSubscriber();
      };
    }
  }, [session]);



  const getAddress = useCallback(async (cep) => {
    try {
      const response = await apiViacep(`${cep}/json`);
      return response.data;
    } catch (error) {
      console.error('Erro ao obter endereço:', error);
      return null;
    }
  }, []);


  return (
    <Dialog header="Meus endereços" className='modal' visible={visible} style={{ width: '50vw' }} onHide={setIsClose}>
      <div>
        {addresses.map((address) => (
          <div key={address.id}>
            <p style={{ color: 'gray', fontSize: 25 }}>Endereço de Entrega</p>
            <p>Cidade: {address.cidade}</p>
            <p>UF: {address.uf}</p>
            <p>Bairro: {address.bairro}</p>
            <p>CEP: {address.cep}</p>
            <p>Número: {address.numero}</p>
            <p>Complemento: {address.complemento}</p>
            <p>Logradouro: {address.logradouro}</p>
            <Button label="Escolher" onClick={() => { setAddressSelected(address); setIsClose(); }} />
          </div>
        ))}
      </div>
      <h1>Adicionar outro</h1>
      <Formik
        initialValues={{
          enderecos: [{
            tipoEndereco: TIPO_ENDERECO.entrega,
            ativo: true,
            cep: '',
            cidade: '',
            bairro: '',
            numero: '',
            complemento: '',
            logradouro: '',
            uf: ''
          }]
        }}
        onSubmit={async (values, { setErrors, setSubmitting, resetForm }) => {
          setLoading(true);
          try {
            await Promise.all(values.enderecos.map(async (endereco) => {
              const addressDB = new AddressDB(session?.token?.id);
              const newAddress = {
                tipoEndereco: TIPO_ENDERECO.entrega,
                cep: endereco.cep,
                cidade: endereco.cidade,
                bairro: endereco.bairro,
                numero: endereco.numero,
                complemento: endereco.complemento,
                logradouro: endereco.logradouro,
                uf: endereco.uf,
                padrao: false,
                ativo: true

              };
              await addressDB.create(newAddress);
              console.log('Endereço cadastrado com sucesso!');
              resetForm();
            }));
          } catch (error) {
            console.error('Erro ao cadastrar endereço:', error);
          }
          setLoading(false);
        }}
      >
        {({ values, handleSubmit, setFieldValue, handleChange }) => (
          <Form onSubmit={handleSubmit}>
            <div className="flex flex-column justify-content-center align-items-center gap-4 mt-7">
              <div className="flex flex-wrap gap-4">
                <FieldArray name="enderecos">
                  {({ push, remove }) => (
                    <>
                      {values.enderecos.map((endereco, index) => (
                        <div key={index} className="flex flex-wrap gap-4">
                          <div className="flex gap-2 align-items-center">
                            <label htmlFor={`enderecos.${index}.cep`}>Cep</label>
                            <InputMask
                              id={`enderecos.${index}.cep`}
                              name={`enderecos.${index}.cep`}
                              mask="99999-999"
                              value={endereco.cep}
                              onChange={handleChange}
                              onBlur={async (e) => {
                                const cep = e.target.value;
                                const addressData = await getAddress(cep);
                                if (addressData) {
                                  setFieldValue(`enderecos.${index}.cidade`, addressData.localidade);
                                  setFieldValue(`enderecos.${index}.uf`, addressData.uf);
                                  setFieldValue(`enderecos.${index}.bairro`, addressData.bairro);
                                  setFieldValue(`enderecos.${index}.logradouro`, addressData.logradouro);
                                }
                              }}
                            />
                          </div>
                          <div className="flex gap-2 align-items-center">
                            <label htmlFor={`enderecos.${index}.cidade`}>Cidade</label>
                            <InputText id={`enderecos.${index}.cidade`} name={`enderecos.${index}.cidade`} value={endereco.cidade} onChange={handleChange} />
                          </div>
                          <div className="flex gap-2 align-items-center">
                            <label htmlFor={`enderecos.${index}.estado`}>UF</label>
                            <InputText id={`enderecos.${index}.estado`} name={`enderecos.${index}.uf`} value={endereco.uf} onChange={handleChange} />
                          </div>
                          <div className="flex gap-2 align-items-center">
                            <label htmlFor={`enderecos.${index}.bairro`}>Bairro</label>
                            <InputText id={`enderecos.${index}.bairro`} name={`enderecos.${index}.bairro`} value={endereco.bairro} onChange={handleChange} />
                          </div>
                          <div className="flex gap-2 align-items-center">
                            <label htmlFor={`enderecos.${index}.rua`}>Logradouro</label>
                            <InputText id={`enderecos.${index}.rua`} name={`enderecos.${index}.rua`} value={endereco.logradouro} onChange={handleChange} />
                          </div>
                          <div className="flex gap-2 align-items-center">
                            <label htmlFor={`enderecos.${index}.numero`}>Número</label>
                            <InputText id={`enderecos.${index}.numero`} name={`enderecos.${index}.numero`} value={endereco.numero} onChange={handleChange} />
                          </div>
                          <div className="flex gap-2 align-items-center">
                            <label htmlFor={`enderecos.${index}.complemento`}>Complemento</label>
                            <InputText id={`enderecos.${index}.complemento`} name={`enderecos.${index}.complemento`} value={endereco.complemento} onChange={handleChange} />
                          </div>

                          <div className="flex flex-wrap justify-content-center gap-3">
                            <Button type="button" onClick={() => push({ cep: '', cidade: '', bairro: '', numero: '', complemento: '', logradouro: '', uf: '' })}>Adicionar outro endereço</Button>
                          </div>
                        </div>
                      ))}
                    </>
                  )}
                </FieldArray>
              </div>
            </div>
            <div className="mt-7 cont-btn">
              <Button
                label="Cadastrar"
                disabled={loading}
                className="btn-cadastrar"
                loading={loading}
                iconPos="right"
                type="submit"
              />
            </div>
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

export default ModalAdress;
