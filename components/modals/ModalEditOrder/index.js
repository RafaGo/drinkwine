import OrderDB from "@/public/database/wrappers/order";
import { useFormik } from "formik";
import router from "next/router";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { Dropdown } from "primereact/dropdown";
import { InputText } from "primereact/inputtext";
import { useEffect } from "react";


const ModalEditOrders = ({ visible, setIsClose, orderData }) => {

  useEffect(() => {
    if (!visible) return formik.resetForm();

    if (orderData) {
      const { numeroPedido, valorTotal, status } = orderData;
      formik.setValues({
        numeroPedido,
        valorTotal,
        status,
      });
    }
  }, [orderData, visible]);

  const formik = useFormik({
    initialValues: {
      numeroPedido: '',
      valorTotal: '',
      status: '',
    },
    onSubmit: async (values) => {
      try {
        const orderDB = new OrderDB();
        await orderDB.update(orderData.id, values);
        console.log('Status Atualizado');
        router.push('/backofficeEstoquista');
      } catch (error) {
        console.error('Erro :', error);
      }
    }
  });


  return (
    <Dialog header="Editar produto" className='modal-edit' visible={visible} style={{ width: '50vw' }} onHide={setIsClose}>
      <div className="p-fluid p-grid p-formgrid">
        <div className="p-field p-col-12">
          <label htmlFor="numeroPedido">Numero Pedido</label>
          <InputText id="numeroPedido" {...formik.getFieldProps('numeroPedido')} disabled />
        </div>
        <div className="p-field p-col-12">
          <label htmlFor="status">Status</label>
          <Dropdown
            id="status"
            value={formik.values.status}
            onChange={(e) => formik.setFieldValue('status', e.value)}
            options={['Aguardando pagamento', 'Pagamento rejeitado', 'Pagamento com sucesso', 'Aguardando retirada', 'Em trânsito', 'Entregue']}
            placeholder="Selecione o status"
          />
        </div>
        <div className="p-field p-col-12">
          <label htmlFor="valorTotal">Valor Total</label>
          <InputText id="valorTotal" {...formik.getFieldProps('valorTotal')} disabled />
        </div>
        <div className="p-field p-col-12">
          <Button label="Salvar" onClick={() => formik.handleSubmit()} />
        </div>
      </div>
    </Dialog>
  );
};

export default ModalEditOrders;
