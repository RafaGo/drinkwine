import { DefaultContext } from "@/contexts/defaultContext";
import ProductDB from "@/public/database/wrappers/product";
import { FormikValues, useFormik } from "formik";
import router from "next/router";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { useCallback, useContext, useEffect } from "react";


const ModalEditProduct = ({ visible, setIsClose, productData }) => {
    const { product } = useContext(DefaultContext);

    useEffect(() => {
        if (!visible) return formik.resetForm();

        if (productData) {
            const { nome, avaliacao, descricao, preco, qtd, image_url, image, imageMain, imageMain_url } = productData;
            formik.setValues({
                nome,
                avaliacao,
                descricao,
                qtd,
                preco,
                image_url,
                image,
                imageMain,
                imageMain_url,
            });
        }
    }, [productData, visible]);

    const formik = useFormik({
        initialValues: {
            nome: '',
            avaliacao: '',
            descricao: '',
            qtd: '',
            preco: '',
            image: [], 
            image_url: [], 
            imageMain: [],
            imageMain_url: []
        },
        onSubmit: async (values) => {
            try {
                const productDB = new ProductDB();
                await productDB.update(productData.id, values);
                console.log('Dados do produto atualizados com sucesso.');
                router.push('/backoffice');
            } catch (error) {
                console.error('Erro ao salvar os dados do usuário:', error);
            }
        }
    });

    const handleMainImage = useCallback((e) => {
        const [file] = Array.from(e.target.files);
        formik.setValues({
            ...formik.values,
            imageMain: file,
            imageMain_url: URL.createObjectURL(file)
        });
    }, [formik.values]);

    const handleOtherImages = useCallback((e) => {
        if (e.target && e.target.files) {
            const files = Array.from(e.target.files);
            const newImageUrls = files.map((file) => URL.createObjectURL(file));

            let updatedImages;
            if (Array.isArray(formik.values.image)) {
                updatedImages = [...formik.values.image, ...files];
            } else {
                updatedImages = [...files];
            }

            formik.setValues({
                ...formik.values,
                image: updatedImages,
                image_url: [...formik.values.image_url, ...newImageUrls],
            });
        }
    }, [formik.values]);  

    const handleDeleteOtherImage = useCallback((index) => {
        const updatedImages = Array.isArray(formik.values.image) ? [...formik.values.image] : [];
        updatedImages.splice(index, 1);
    
        const updatedImageUrls = Array.isArray(formik.values.image_url) ? [...formik.values.image_url] : [];
        updatedImageUrls.splice(index, 1);
    
        formik.setValues({
            ...(formik.values),
            image: updatedImages ,
            image_url: updatedImageUrls,
        });
    }, [formik.values]);
    return (
        <Dialog header="Editar produto" className='modal-edit' visible={visible} style={{ width: '50vw' }} onHide={setIsClose}>
            <div className="p-fluid p-grid p-formgrid">
                <div className="p-field p-col-12">
                    <label htmlFor="nome">Nome</label>
                    <InputText id="nome" {...formik.getFieldProps('nome')} />
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="avaliacao">Avaliação</label>
                    <InputText id="avaliacao" {...formik.getFieldProps('avaliacao')} />
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="preco">Preço</label>
                    <InputText id="preco" {...formik.getFieldProps('preco')} />
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="preco">Quantidade</label>
                    <InputText id="qtd" {...formik.getFieldProps('qtd')} />
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="descricao">Descrição</label>
                    <InputText id="descricao" {...formik.getFieldProps('descricao')} />
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="imageMain_url">Imagem Principal</label>
                    <input id="imageMain_url" type="file" accept="image/*" onChange={handleMainImage} />
                    {formik.values.imageMain_url && (
                        <img src={formik.values.imageMain_url} alt="Imagem principal" />
                    )}
                </div>
                <div className="p-field p-col-12">
                    <label htmlFor="outras_imagens">Outras Imagens</label>
                    <input id="outras_imagens" type="file" accept="image/*" multiple onChange={handleOtherImages} />
                    {Array.isArray(formik.values.image_url) && formik.values.image_url.map((imageUrl, index) => (
                        <div key={index} className="image-container">
                            <img src={imageUrl} alt={`Imagem ${index}`} />
                            <button onClick={() => handleDeleteOtherImage(index)}>Deletar</button>
                        </div>
                    ))}
                </div>
                <div className="p-field p-col-12">
                    <Button label="Salvar" onClick={() => formik.handleSubmit()} />
                </div>
            </div>
        </Dialog>
    );
};

export default ModalEditProduct;
