import { DefaultContext } from "@/contexts/defaultContext";
import ProductDB from "@/public/database/wrappers/product";
import { FormikValues, useFormik } from "formik";
import router from "next/router";
import { Button } from "primereact/button";
import { useCallback, useContext, useEffect } from "react";
import '../../../pages/app.css';
import './index.css';
import { Rating } from 'primereact/rating';
import { Carousel } from 'primereact/carousel';
import CartContext from "@/contexts/cartContext";
import { Dialog } from "primereact/dialog";

const productTemplate = (item) => {
    return (
        <div className="product-item">
            <img src={item} alt="Product" />
        </div>
    );
};

const ModalViewProduct = ({ visible, setIsClose, productDataView }) => {
    const { product } = useContext(DefaultContext);
    const { addProductInCart } = useContext(CartContext)

    useEffect(() => {
        if (!visible) return formik.resetForm();

        if (productDataView) {
            const { nome, avaliacao, descricao, preco, image_url, image, imageMain, imageMain_url } = productDataView;
            formik.setValues({
                nome,
                avaliacao,
                descricao,
                preco,
                image_url,
                image,
                imageMain,
                imageMain_url,
            });
        }
    }, [productDataView, visible]);

    const formik = useFormik({
        initialValues: {
            nome: '',
            avaliacao: '',
            descricao: '',
            preco: '',
            image: [],
            image_url: [],
            imageMain: [],
            imageMain_url: []
        },
        onSubmit: async (values) => {
            try {
                const productDB = new ProductDB();
                await productDB.update(productDataView.id, values);
                console.log('Dados do produto atualizados com sucesso.');
                router.push('/backoffice');
            } catch (error) {
                console.error('Erro ao salvar os dados do usuário:', error);
            }
        }
    });

    const handleMainImage = useCallback((e) => {
        const [file] = Array.from(e.target.files);
        formik.setValues({
            ...formik.values,
            imageMain: file,
            imageMain_url: URL.createObjectURL(file)
        });
    }, [formik.values]);

    const handleOtherImages = useCallback((e) => {
        if (e.target && e.target.files) {
            const files = Array.from(e.target.files);
            const newImageUrls = files.map((file) => URL.createObjectURL(file));

            let updatedImages;
            if (Array.isArray(formik.values.image)) {
                updatedImages = [...formik.values.image, ...files];
            } else {
                updatedImages = [...files];
            }

            formik.setValues({
                ...formik.values,
                image: updatedImages,
                image_url: [...formik.values.image_url, ...newImageUrls],
            });
        }
    }, [formik.values]);

    const handleDeleteOtherImage = useCallback((index) => {
        const updatedImages = Array.isArray(formik.values.image) ? [...formik.values.image] : [];
        updatedImages.splice(index, 1);

        const updatedImageUrls = Array.isArray(formik.values.image_url) ? [...formik.values.image_url] : [];
        updatedImageUrls.splice(index, 1);

        formik.setValues({
            ...(formik.values),
            image: updatedImages,
            image_url: updatedImageUrls,
        });
    }, [formik.values]);
    return (
        <Dialog className='view-a-product' visible={visible} onHide={setIsClose}>
            <div className="cont-product">
                <div className=" flex align-items-center mb-5">
                    <div className="p-field p-col-12">
                        {formik.values.imageMain_url && (
                            <img src={formik.values.imageMain_url} alt="Imagem principal" />
                        )}
                    </div>
                    <div className="flex flex-column align-items-center ml-7">
                        <label>R${formik.values.preco}</label>
                        <Button  label="COMPRAR" className="btn-comprar mt-5" />
                    </div>
                </div>
                <div>
                    <label>{formik.values.nome}</label>
                    <div className="mt-5">
                        <h2>Avaliação:</h2>
                        <Rating value={formik.values.avaliacao} readOnly cancel={false} />
                    </div>
                    <div className="mt-5">
                        <h2>Descrição:</h2>
                        <p>{formik.values.descricao}</p>
                    </div>
                </div>
                <div className="mt-5">
                    <Carousel
                        value={formik.values.image_url}
                        numVisible={1}
                        showIndicators={false}
                        numScroll={1}
                        itemTemplate={productTemplate}
                    />
                </div>
            </div>
        </Dialog>
    );
};

export default ModalViewProduct;
