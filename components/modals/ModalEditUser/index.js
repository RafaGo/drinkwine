import { DefaultContext } from '@/contexts/defaultContext';
import UserDB from '@/public/database/wrappers/user';
import { hashSync, compareSync } from 'bcryptjs';
import { useFormik } from 'formik';
import router from 'next/router';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import React, { useContext, useEffect } from 'react';
import { InputMask } from 'primereact/inputmask';
import '../../../pages/app.css';
import './index.css';



const ModalEditUser = ({ visible, setIsClose, userData }) => {
    const { user } = useContext(DefaultContext);

    function TestaCPF(strCPF) {
        var Soma;
        var Resto;
        Soma = 0;
        strCPF = strCPF.replace(/[^0-9]/g, '');
        if (strCPF == '00000000000') return false;

        for (var i = 1; i <= 9; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if (Resto == 10 || Resto == 11) Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10))) return false;

        Soma = 0;
        for (var i = 1; i <= 10; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if (Resto == 10 || Resto == 11) Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11))) return false;
        return true;
    }

    const validate = async (values) => {
        values.cpf = values.cpf.replace(/[^0-9]/g, '');
        let cpfMask;
        cpfMask = values.cpf.match(/(\d{3})(\d{3})(\d{3})(\d{2})/);
        if (cpfMask !== null) values.cpf = `${cpfMask[1]}.${cpfMask[2]}.${cpfMask[3]}-${cpfMask[4]}`;
        const errors = {};

        if (!values.nome) {
            errors.nome = 'Este campo é necessário';
        } else if (values.nome.length < 3) {
            errors.nome = 'O nome precisa ter 3 caracteres ou mais';
        }
        if (values.senha !== values.confirmarSenha) {
            errors.confirmarSenha = 'As senhas não coincidem';
        }
        if (!values.cpf) {
            errors.cpf = 'Este campo é necessário';
        } else if (cpfMask == null) {
            errors.cpf = 'Informe o CPF corretamente';
        } else if (TestaCPF(values.cpf) == false) errors.cpf = 'Informe um CPF válido';
        if (!values.senha) {
            errors.senha = 'Este campo é necessário';
        } else if (values.senha.length < 6) {
            errors.senha = 'A senha precisa ter 6 caracteres';
        }

        if (!compareSync(values.senhaAtual, userData.senha)) {
            errors.senhaAtual = 'Senha atual incorreta';
        }

        return errors;
    };

    const desabilitaGrupo = user && user.id === (userData && userData.id);
    
    useEffect(() => {
        if (!visible) return formik.resetForm();

        if (userData) {
            const { email, nome, cpf, grupo, confirmarSenha } = userData;
            formik.setValues({
                nome,
                email,
                cpf,
                grupo,
                senha: '',
                senhaAtual: '',
                confirmarSenha,
            });
        }
    }, [userData, visible]);


    const formik = useFormik({
        initialValues: {
            nome: '',
            cpf: '',
            email: '',
            grupo: '',
            senha: '',
            confirmarSenha: '',
            senhaAtual: '', 
        },
        validate,
        onSubmit: async (values) => {
            try {
                const senhaHash = hashSync(values.senha, 10);

                const updatedUserData = { ...values, senha: senhaHash };

                const { confirmarSenha, senhaAtual, ...userDataWithoutConfirmarSenha } = updatedUserData;

                const userDB = new UserDB();
                await userDB.update(userData.id, userDataWithoutConfirmarSenha);
                console.log('Dados do usuário atualizados com sucesso.');
                router.push('/backoffice');
            } catch (error) {
                console.error('Erro ao salvar os dados do usuário:', error);
            }
        }
    });

    return (
        <div>
            <Dialog header="Editar usuário" className='modal-edit' visible={visible} style={{ width: '50vw' }} onHide={setIsClose}>
                <form onSubmit={formik.handleSubmit}>
                    <div className="flex gap-4 flex-wrap justify-content-center mt-6">
                        <div className="flex flex-column gap-2">
                            <label htmlFor="nome">Nome</label>
                            <InputText id="nome" value={formik.values.nome} onChange={formik.handleChange} />
                            <p>{formik.errors.nome}</p>
                        </div>

                        <div className="flex flex-column gap-2">
                            <label htmlFor="email">E-mail</label>
                            <InputText id="email" value={formik.values.email} disabled />
                        </div>

                        <div className="flex flex-column gap-2">
                            <label htmlFor="cpf">Cpf</label>
                            <InputMask mask="999.999.999-99" id="cpf" value={formik.values.cpf} onChange={formik.handleChange} />
                        </div>

                        <div className="flex flex-column gap-2">
                            <label htmlFor="senhaAtual">Senha atual</label>
                            <Password
                                id="senhaAtual"
                                value={formik.values.senhaAtual}
                                onChange={(e) => formik.setFieldValue('senhaAtual', e.target.value)}
                                feedback={false}
                                toggleMask
                            />
                            <p>{formik.errors.senhaAtual}</p>
                        </div>

                        <div className="flex flex-column gap-2">
                            <label htmlFor="senha">Nova senha</label>
                            <Password
                                id="senha"
                                value={formik.values.senha}
                                onChange={(e) => formik.setFieldValue('senha', e.target.value)}
                                feedback={false}
                                toggleMask
                            />
                            <p>{formik.errors.senha}</p>
                        </div>

                        <div className="flex flex-column gap-2">
                            <label htmlFor="confirmarSenha">Confirmar senha</label>
                            <Password
                                id="confirmarSenha"
                                value={formik.values.confirmarSenha}
                                onChange={(e) => formik.setFieldValue('confirmarSenha', e.target.value)}
                                feedback={false}
                                toggleMask
                            />
                            <p>{formik.errors.confirmarSenha}</p>
                        </div>
                    </div>
                    <div className="flex justify-content-center mt-5">
                        <Dropdown
                            name="grupo"
                            id="grupo"
                            className="select-group"
                            value={formik.values.grupo}
                            disabled={desabilitaGrupo}
                            onChange={formik.handleChange}
                            options={[
                                { label: 'Estoquista', value: 'estoquista' },
                                { label: 'Administrador', value: 'admin' },
                            ]}
                        />
                    </div>

                    <div className="mt-7 flex gap-4 cont-btns">
                        <Button type="button" label="Cancelar" className="btn-cancelar" onClick={setIsClose} />
                        <Button type="submit" label="Salvar" className="btn-salvar" />
                    </div>
                </form>
            </Dialog>
        </div>
    );
};

export default ModalEditUser;
