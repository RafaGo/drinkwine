import { DefaultContext } from "@/contexts/defaultContext";
import Address from "@/public/database/entities/address.entity";
import AddressDB from "@/public/database/wrappers/address";
import ClientDB from "@/public/database/wrappers/client";
import { FormikValues, useFormik } from "formik";
import router from "next/router";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { InputSwitch } from "primereact/inputswitch";
import { useCallback, useContext, useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { TIPO_ENDERECO } from "@/types/tipoEndereco";


const ModalEditEndereco = ({ visible, setIsClose, enderecoData }) => {
  const { data: session } = useSession();
  const [address, setAddress] = useState();
  useEffect(() => {
    if (!session) return;

    const onSubscriber = new AddressDB(session?.token?.id).on(end => {
      setAddress(end.filter((end) => end.tipoEndereco === TIPO_ENDERECO.entrega));
    });

    return () => {
      onSubscriber();
    }


  }, [session])

  useEffect(() => {
    if (!enderecoData) return;

    const { end } = enderecoData;

    formik.setValues({
      cidade: end?.cidade,
      uf: end?.uf,
      bairro: end?.bairro,
      cep: end?.cep,
      numero: end?.numero,
      complemento: end?.complemento,
      logradouro: end?.logradouro,
      padrao: end?.padrao ?? false,
    })
  }, [enderecoData])
  const formik = useFormik({
    initialValues: {
      cidade: '',
      uf: '',
      bairro: '',
      cep: '',
      numero: '',
      complemento: '',
      logradouro: '',
      padrao: false,
    },
    onSubmit: async (values) => {
      try {
        const addressDB = new AddressDB(enderecoData.idClient);
        await addressDB.update(enderecoData.end.id, values);
        console.log('Dados do endereço atualizados com sucesso.');
        setIsClose();
      } catch (error) {
        console.error('Erro ao salvar os dados do endereço:', error);
      }
    }
  });

  useEffect(() => {
    if (!visible) return formik.resetForm();

  }, [enderecoData, visible]);
  return (
    <Dialog header="Editar endereço" className='modal-edit' visible={visible} style={{ width: '50vw' }} onHide={setIsClose}>
      <form onSubmit={formik.handleSubmit}>

        <div className="p-fluid p-grid p-formgrid">
          <div className="p-field p-col-12">
            <label htmlFor="cidade">Cidade</label>
            <InputText id="cidade" {...formik.getFieldProps('cidade')} disabled />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="uf">UF</label>
            <InputText id="uf" {...formik.getFieldProps('uf')} disabled />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="bairro">Bairro</label>
            <InputText id="bairro" {...formik.getFieldProps('bairro')} disabled />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="cep">CEP</label>
            <InputText id="cep" {...formik.getFieldProps('cep')} disabled />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="numero">Número</label>
            <InputText id="numero" {...formik.getFieldProps('numero')} disabled />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="complemento">Complemento</label>
            <InputText id="complemento" {...formik.getFieldProps('complemento')} disabled />
          </div>
          <div className="p-field p-col-12">
            <label htmlFor="logradouro">Logradouro</label>
            <InputText id="logradouro" {...formik.getFieldProps('logradouro')} disabled />
          </div>

          <div className="p-field p-col-12">
            <Button label="Editar" type="submit" />
          </div>
        </div>
      </form>
    </Dialog>
  );
};

export default ModalEditEndereco;
