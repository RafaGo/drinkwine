import { useContext, useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { Button } from "primereact/button";
import { useSession } from "next-auth/react";
import CartContext from "@/contexts/cartContext";
import ResumeOrder from "../ResumeOder/index";
import "../Payment/style.css";
import { InputText } from "primereact/inputtext";
import { InputMask } from "primereact/inputmask";

const Payment = ({ address, products, freteSelecionado, tipoFrete, dataEntrega }) => {
  const { totalComFrete } = useContext(CartContext);
  const [metodoPagamento, setMetodoPagamento] = useState("");
  const [dadosCartao, setDadosCartao] = useState({
    nome: "",
    numero: "",
    validade: "",
    cvv: ""
  });
  const [dadosBoleto, setDadosBoleto] = useState({
    nome: "",
    cpf: ""
  });
  const [parcelas, setParcelas] = useState(1);
  const [parcelaSelecionada, setParcelaSelecionada] = useState(1);
  const { data: session } = useSession();
  const [modeResume, setModeResume] = useState(false);
  const [modePayment, setModePayment] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const handleMetodoPagamentoChange = (metodo) => {
    setMetodoPagamento(metodo);
    setShowAlert(false);
  };

  const handleDadosCartaoChange = (event) => {
    const { name, value } = event.target;
    setDadosCartao((prevData) => ({
      ...prevData,
      [name]: value
    }));
  };

  const handleDadosBoletoChange = (event) => {
    const { name, value } = event.target;
    setDadosBoleto((prevData) => ({
      ...prevData,
      [name]: value
    }));
  };

  const handleParcelasChange = (event) => {
    const selectedParcela = parseInt(event.target.value);
    setParcelas(selectedParcela);
    setParcelaSelecionada(selectedParcela);
  };

  const handleResume = () => {
    if (session && metodoPagamento !== "") {
      setModeResume(true);
    } else {
      setShowAlert(true);
    }
  };

  const handleReturnToPayment = () => {
    setModeResume(false);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);


  return (
    <div className="flex flex-column align-items-center" style={{ background: "#1E1E1E" }}>
      {showAlert && (
        <div className="message mt-5" role="alert">
          <i className="pi pi-exclamation-triangle" style={{ fontSize: "1.5rem" }}></i>
          <p>Selecione uma forma de pagamento antes de prosseguir!</p>
        </div>
      )}
      {!modeResume &&
        <Card className="cont-payment">
          <div className="card-payment mt-7">
            <h1>Forma de pagamento</h1>
            <form className="flex flex-column gap-4 mt-6">
              <div className="payment-cartao" onChange={() => handleMetodoPagamentoChange("cartao")}>
                <div className="flex justify-content-between w-full">
                  <label className="w-5 cursor-pointer">
                    <input
                      className="mr-2 w-1 cursor-pointer"
                      type="radio"
                      name="metodoPagamento"
                      value="cartao"
                      checked={metodoPagamento === "cartao"}
                    />
                    Pagar com Cartão de crédito
                  </label>
                  <i className="pi pi-credit-card" style={{ fontSize: "1.3rem" }}></i>
                </div>
                {metodoPagamento === "cartao" && (
                  <div>
                    <div className="flex gap-4 mt-5">
                      <div className="card flex justify-content-center w-full">
                        <div className="flex flex-column gap-2 w-full">
                          <label htmlFor="nameCard">Nome no Cartão</label>
                          <InputText id="nameCard" name="nome" value={dadosCartao.nome} onChange={handleDadosCartaoChange} style={{width: "100%"}}/>
                        </div>
                      </div>
                      <div className="card flex justify-content-center w-full">
                        <div className="flex flex-column gap-2 w-full">
                          <label htmlFor="number">Número do Cartão</label>
                          <InputMask id="number" value={dadosCartao.numero} onChange={handleDadosCartaoChange} mask="9999 9999 9999 9999" style={{width: "100%"}} />
                        </div>
                      </div>
                    </div>
                    <div className="flex gap-4 mt-3 w-full">
                      <div className="card flex justify-content-center w-full">
                        <div className="flex flex-column gap-2 w-full">
                          <label htmlFor="validity">Validade</label>
                          <InputMask id="validity" value={dadosCartao.validade} onChange={handleDadosCartaoChange} mask="99/99" style={{width: "100%"}} />
                        </div>
                      </div>
                      <div className="card flex justify-content-center w-full">
                        <div className="flex flex-column gap-2 w-full">
                          <label htmlFor="cvv">CVV</label>
                          <InputMask id="cvv" value={dadosCartao.cvv} onChange={handleDadosCartaoChange} mask="999" style={{width: "100%"}} />
                        </div>
                      </div>
                      <div className="card flex justify-content-center w-full">
                        <div className="flex flex-column gap-2 w-full">
                          <label>Número de Parcelas</label>
                          <select value={parcelas} onChange={handleParcelasChange}>
                            <option value={1}>1x á vista R${totalComFrete}</option>
                            <option value={2}>2x de R${(totalComFrete / 2).toFixed(2)}</option>
                            <option value={3}>3x de R${(totalComFrete / 3).toFixed(2)}</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className="payment-boleto flex flex-column" onChange={() => handleMetodoPagamentoChange("boleto")}>
                <div className="flex justify-content-between w-full">
                  <label className="w-5 cursor-pointer">
                    <input
                      className="mr-2 w-1 cursor-pointer"
                      type="radio"
                      name="metodoPagamento"
                      value="boleto"
                      checked={metodoPagamento === "boleto"}
                    />
                    Pagar com Boleto
                  </label>
                  <i className="pi pi-qrcode" style={{ fontSize: "1.3rem" }}></i>
                </div>
                {metodoPagamento === "boleto" && (
                  <div className="w-full">
                    <div className="flex gap-4 mt-5 w-full">
                      <div className="card flex justify-content-center w-full">
                        <div className="flex flex-column gap-2 w-full">
                          <label htmlFor="name">Nome</label>
                          <InputText id="name" name="nome" value={dadosBoleto.nome} onChange={handleDadosBoletoChange} style={{width: "100%"}}/>
                        </div>
                      </div>
                      <div className="card flex justify-content-center w-full">
                        <div className="flex flex-column gap-2 w-full">
                          <label htmlFor="cpf">CPF</label>
                          <InputMask id="cpf" name="cpf" value={dadosBoleto.cpf} onChange={handleDadosBoletoChange} mask="999.999.999-99" style={{width: "100%"}}/>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </form>
          </div>
          <div className="card-value mt-7 flex flex-column justify-content-between">
            <div>
              <h1>Valor da compra</h1>
              <div className="flex justify-content-between align-items-center mt-5">
                <h2>Total da compra:</h2>
                <p>{Number.isInteger(totalComFrete) ? `${totalComFrete}.00` : totalComFrete}</p>
              </div>
              {metodoPagamento === "cartao" && (
                <div className="flex justify-content-between align-items-center mt-2 border-top-1">
                  <h2>Total a prazo:</h2>
                  <div className="flex flex-column align-items-end">
                    <p>{Number.isInteger(totalComFrete) ? `${totalComFrete}.00` : totalComFrete.toFixed(2)}</p>
                    <label>(em {parcelas}x de R$ {(totalComFrete / parcelas).toFixed(2)} sem juros)</label>
                  </div>
                </div>
              )}
            </div>
            <div className="cont-btn">
              <Button
                className="btn-resumo"
                label="Resumo do pedido"
                onClick={handleResume}
              />
            </div>
          </div>
        </Card>
      }
      {modeResume && (
        <ResumeOrder
          address={address}
          products={products}
          parcelas={parcelas}
          valorParcelado={(totalComFrete / parcelas).toFixed(2)}
          metodoPagamento={metodoPagamento}
          freteSelecionado={freteSelecionado}
          tipoFrete={tipoFrete}
          dataEntrega={dataEntrega}
          onReturnToPayment={handleReturnToPayment}
        />
      )}
    </div>
  );
};

export default Payment;
