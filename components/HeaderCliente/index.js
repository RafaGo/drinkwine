import router from "next/router";
import "./index.css";
import { Avatar } from "primereact/avatar";
import { Menu } from "primereact/menu";
import { useContext, useRef, useState } from "react";
import { signOut } from "next-auth/react";
import editAddress from "@/pages/editAddress";
import ModalEditClient from "../modals/ModalEditClient";
import EditAddress from "@/pages/editAddress";
import CartContext from "@/contexts/cartContext";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { Button } from "primereact/button";

const HeaderCliente = ({ userName }) => {
  const [userInitial, setUserInitial] = useState(userName ? userName.charAt(0).toUpperCase() : "");
  const [openClient, setOpenClient] = useState(false);
  const { qtdInCart } = useContext(CartContext);

  const handleSignOut = () => {
    signOut({ callbackUrl: "/" });
  };

  const goPurchase = () => {
    router.push("/carrinho");
  };

  const goEditAddress = () => {
    router.push("/editAddress");
  };

  const goRequests = () => {
    router.push("/requests");
  };

  const editClient = () => {
    setOpenClient(true);
  };

  const accept = () => {
    handleSignOut();
    router.push("/");
  };
  
  const reject = () => {
  };

  const confirm = () => {
    confirmDialog({
      group: "headless",
      message: "Tem certeza que deseja sair?",
      header: "Confirmação",
      icon: "pi pi-exclamation-triangle",
      defaultFocus: "accept",
      accept,
      reject
    });
  };

  const handleLogin = () => {
    router.push("/login");
  };

  const menuLeft = useRef(null);
  const menuRight = useRef(null);
  const toast = useRef(null);
  const items = [
    {
      label: `Olá ${userName}`,
      items: [
        {
          label: "Perfil",
          icon: "pi pi-user",
          command: editClient
        },
        {
          label: "Editar endereço",
          icon: "pi pi-home",
          command: goEditAddress
        },
        {
          label: "Carrinho",
          icon: "pi pi-shopping-cart",
          command: goPurchase
        },
        {
          label: "Pedidos",
          icon: "pi pi-ticket",
          command: goRequests
        },
        {
          label: "Sair",
          icon: "pi pi-sign-out",
          command: confirm
        },
      ]
    }
  ];
  const IsUser = () => {
    if (userName === "Usuário") {
      return (
        <header className='header'>
          <a href="/">
            <img src="images\logo.png" alt="Drink Wine" />
          </a>

          <div className="card flex divHeader gap-4">
            <a href="/login"><i className="pi pi-user" style={{ fontSize: "1.5rem" }}></i>Faça Login</a>
            <a href="/cadastroCliente"><i className="pi pi-user-plus" style={{ fontSize: "1.5rem" }}></i>Cadastre-se</a>
            <a href="/carrinho">
              <i className="pi pi-shopping-cart" style={{ fontSize: "1.5rem" }}></i>
              {qtdInCart !== 0 && (
                <span>{qtdInCart}</span>
              )}
            </a>
          </div>
        </header>
      );
    }
    return (
      <div className='header'>
        <a href="/">
          <img src="images\logo.png" alt="teste" />
        </a>
        <div className="card flex divHeader gap-4">
          <div className="card flex justify-content-center ">
            <a href="/carrinho">
              <i className="pi pi-shopping-cart" style={{ fontSize: "3.5rem" }}></i>
              {qtdInCart !== 0 && (
                <span>{qtdInCart}</span>
              )}
            </a>
          </div>
          <Menu model={items} popup ref={menuRight} id="popup_menu_right" popupAlignment="right" className='mt-2' />
          <Avatar label={userInitial} style={{ backgroundColor: "#9c27b0", color: "#ffffff" }} shape="circle"
            onClick={(event) => menuRight.current.toggle(event)} />
        </div>
        <ConfirmDialog
                group="headless"
                content={({ headerRef, contentRef, footerRef, hide, message }) => (
                    <div className="flex flex-column align-items-center p-5 surface-overlay border-round">
                      <div className="flex gap-2 align-items-center mb-3">
                        <i className="pi pi-question-circle text-3xl" style={{color: "#440C0C"}}></i>
                        <p className="m-0 message-dialog" ref={contentRef}>
                            {message.message}
                        </p>
                      </div>
                        <div className="flex align-items-center gap-2 mt-4" ref={footerRef}>
                            <Button
                                label="Sim"
                                onClick={(event) => {
                                    hide(event);
                                    accept();
                                }}
                                className="w-8rem"
                                style={{background: "#440C0C",borderColor: "#440C0C", color: "#fff"}}
                            ></Button>
                            <Button
                                label="Não"
                                outlined
                                onClick={(event) => {
                                    hide(event);
                                    reject();
                                }}
                                className="w-8rem"
                                style={{borderColor: "#440C0C", color: "#440C0C"}}
                            ></Button>
                        </div>
                    </div>
                )}
            />
      </div>
    );

  };
  

  return (
    <>
      <IsUser />
      <ModalEditClient
        visible={openClient}
        setIsClose={() => setOpenClient(false)}
      />
    </>
  );
};

export default HeaderCliente;
